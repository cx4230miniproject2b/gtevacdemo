package model.strategy;

import model.CarTrafficEvacuationModel;
import model.Road;
import model.util.AStarSearchHeuristic;
import desmoj.core.dist.DiscreteDistEmpirical;
import desmoj.core.dist.UniformRandomGenerator;
import desmoj.core.simulator.Model;


/**
 * Default strategy for crossing intersections with no officers.
 * Intersection treated as a 4-way stop.
 * @author Lauren Winston
 *
 */
public class IntersectionStrategy extends Strategy {

	private int currentRoadIndex = -1;
	private int directionIndex = -1;
	private boolean needChange = false;
	final private DiscreteDistEmpirical<Integer> dist;
	private double[] probabilities;
	private Double[] valid;

    public IntersectionStrategy(Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
        final UniformRandomGenerator rng = ((CarTrafficEvacuationModel)
                this.getModel()).getRNG();
        dist = new DiscreteDistEmpirical<Integer>(owner, name, showInTrace, showInTrace);
        dist.changeRandomGenerator(rng); 
        dist.addEntry(0, 0);
        dist.addEntry(1, 0);
        dist.addEntry(2, 0);
        dist.addEntry(3, 0);
        probabilities = new double[4];
    }
    
    /* Because this is treated as a 4-way stop, the road will change everytime, unless no other entering roads
     * have cars
     */
    @Override
    public boolean needsChange() {
    	return needChange;
    }
    
    @Override
    public Road getNext() {
    	if (currentRoadIndex < 0 || !hasValidExits()) {
    		return null;
    	} 
    	Road currentRoad = in[currentRoadIndex];
        return currentRoad;
    }
    
    @Override
    public void notifyInFlow(Road road) {
    	if (currentRoadIndex == -1 || directionIndex == -1) notifyCrossing();
    }
    
    @Override
    public void notifyOutFlow(Road road) {
        if (currentRoadIndex == -1 || directionIndex == -1) notifyCrossing();
    }
    
    @Override
    public Road getDirection(Road road) { 
    	if (directionIndex == -1) return null;
    	return out[directionIndex];
    	
    }
    
    
    @Override
    public boolean hasValidExits() {
    	for (int i = 0; i < out.length; i++) {
    		Road exit = out[i];
    		if (valid[i] != null && !exit.isFull()) {
    			return true;
    		}
    	}
    	return false;
    }
    
    public void notifyCrossing() {
        int prev = currentRoadIndex;
        currentRoadIndex = rotateRoads(currentRoadIndex);
        if (prev == currentRoadIndex) {
            needChange = false;
        } else {
            needChange = true;
        }
        // pruning step, should never go to a place location farther away from exit
        if (valid == null) {
            valid = new Double[4];
            AStarSearchHeuristic heuristic = ((CarTrafficEvacuationModel) this.getModel()).getHeuristic();
            for (int i = 0; i < 4; i++) {
                if (out[i] != null) {
                    double current = heuristic.costFrom(out[i].getSource());
                    double cost = heuristic.costFrom(out[i].getDestination());
                    double edge = out[i].getEstimatedMean();
                    double val = cost + edge;
                    if (val <= current) {
                        valid[i] = val;
                    }
                }
            }
        }
        double probSum = 0;
        double[] currentProbabilities = new double[4];
        for (int i = 0; i < out.length; i++) {
            if (valid[i] != null && i != currentRoadIndex) {
                Road road = out[i];
                double cost = valid[i];
                double percentA = 1 - (double)(road.getCount())/ ((double)road.getCapacity());
                double chanceA =(percentA/cost);
                currentProbabilities[i] = chanceA;
                probSum += chanceA;
            } else {
                currentProbabilities[i] = 0;
            }
        }
        if (probSum == 0) {
            directionIndex = -1;
        } else {
            UniformRandomGenerator rng = ((CarTrafficEvacuationModel)
                    this.getModel()).getRNG();
            double rand = rng.nextDouble();
            boolean found = false;
            for (int i = 0; i < 4; i++) {
                double prob = currentProbabilities[i]/probSum;
                currentProbabilities[i] = prob;
                if (!found) {
                    if (rand < prob) {
                        directionIndex = i;
                        found = true;
                    } else {
                        rand -= prob;
                    }
                }
            }
            probabilities = currentProbabilities;
        }
        /*
        boolean shouldSample = false;
        for (int i = 0; i < currentProbabilities.length; i++) {
            currentProbabilities[i] /= probSum;
            if (currentProbabilities[i] > 0 && i != currentRoadIndex) {
                dist.removeEntry(i); 
                dist.addEntry(i, currentProbabilities[i]);
                probabilities[i] = currentProbabilities[i];
                shouldSample = true;
            }
        }
        if (shouldSample) {
            directionIndex = dist.sample();
        } else {
            // only if all valid exits are full
            directionIndex = -1;
        }
        */
    }
}
