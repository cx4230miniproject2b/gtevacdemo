package model.strategy;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import model.Road;
import desmoj.core.dist.DiscreteDistPoisson;
import desmoj.core.dist.DiscreteDistUniform;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;

/**
 * A strategy for deciding which car crosses next at an intersection.
 * 
 * A strategy does not change the state of the simulation, but merely
 * provides a plan for the simulation to advance. The actual state changes
 * caused by crossing should be handled by the model itself.
 * 
 * @author Brian Dong
 */
public abstract class Strategy extends Entity {
    
    // Strategy ids
    public static final int MOST_CARS_OFFICER = 1;
    public static final int NUM_CARS_OFFICER = 2;
    public static final int TIMER_OFFICER = 3;
    public static final int NO_OFFICER =4;
    
    /** The roads entering the intersection this belongs to. */
    protected Road[] in;
    /** The roads exiting the intersection this belongs to. */
    protected Road[] out;
    
    public static Strategy NewStrategy(Model owner, String name, boolean showInTrace, int strategy_id) {
    	Strategy s;
    	if (strategy_id == MOST_CARS_OFFICER) {
    		s = new MostCarsOfficerStrategy(owner, name, showInTrace);
    	} else if (strategy_id == NUM_CARS_OFFICER) {
    		DiscreteDistPoisson dist = new DiscreteDistPoisson(owner, name, NumCarsOfficerStrategy.avg_cars_leaving, showInTrace, showInTrace);
    		s = new NumCarsOfficerStrategy(owner, name, showInTrace, dist);
    	} else if (strategy_id == TIMER_OFFICER) {
    		DiscreteDistPoisson dist = new DiscreteDistPoisson(owner, name, 5 * NumCarsOfficerStrategy.avg_cars_leaving, showInTrace, showInTrace);
    		s = new TimerOfficerStrategy(owner, name, dist, showInTrace);
    	} else {
    		s = new IntersectionStrategy(owner, name, showInTrace);
    	}
    	
    	return s;
    }
    
    public Strategy(Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
    }
    
    /**
     * Sets the entering roads for this strategy to consider. The roads should
     * be an array [N, E, S, W], with null if a road does not exist.
     * 
     * This is only called when setting the roads for an intersection.
     * 
     * @param roads An array of roads.
     */
    public void setEnteringRoads(Road[] roads) {
        /*
         * Note: This is direct access to the road array since it should be
         * the same road[] referenced by the intersection.
         */
        this.in = roads;
    }
    
    /**
     * Sets the exiting roads for this strategy to consider. The roads should
     * be an array [N, E, S, W], with null if a road does not exist.
     * 
     * This is only called when setting the roads for an intersection.
     * 
     * @param roads An array of roads.
     */
    public void setExitingRoads(Road[] roads) {
        /*
         * Note: This is direct access to the road array since it should be
         * the same road[] referenced by the intersection.
         */
        this.out = roads;
    }
    
    protected Road[] getEnteringRoads() {
        return this.in;
    }
    
    protected Road[] getExitingRoads() {
        return this.out;
    }
    
    public boolean isEntranceNull() {
    	return in == null;
    }

    public boolean isExitNull() {
    	return out == null;
    }
    
    /**
     * Returns the road whose car gets to cross next in
     * the intersection. Returns null if all cars decide to wait.
     * 
     * @return  The roads whose car gets to cross next in the
     *          intersection, null if all cars decide to wait.
     */
    public abstract Road getNext();
    
    /**
     * Returns the intended direction of travel for a car at the front
     * of an entering road.
     * 
     * @param road  An entering road.
     * @return  An exiting road.
     */
    public abstract Road getDirection(Road road);
    
    /**
     * Returns true if this strategy represents the presence of an officer,
     * otherwise returns false.
     * 
     * @return  True if this strategy represents the presence of an officer,
     *          false otherwise.
     */
    public boolean hasOfficer() {
        return this instanceof OfficerStrategy;
    }
    
    /**
     * Returns whether the intersection this belongs to has valid exits.
     * 
     * @return  True if the intersection of this belongs to has valid exits,
     *          false otherwise.
     */
    public abstract boolean hasValidExits(); 
    
    /**
     * Returns whether the next crossing involves changing the current
     * direction of traffic.
     * 
     * @return  True if the next crossing changes the current direction of
     *          traffic, false otherwise.
     */
    public abstract boolean needsChange();
    
    /**
     * Notify this strategy of incoming traffic flow.
     * 
     * @param road  A road which has recently updated.
     */
    public abstract void notifyInFlow(Road road);
    
    /**
     * Notify this strategy of potential outgoing traffic flow.
     * 
     * @param road  A road which has recently updated.
     */
    public abstract void notifyOutFlow(Road road);
    
    /**
     * Notifies this strategy of a crossing.
     */
    public abstract void notifyCrossing();
    
    /**
     * Returns a copy of this strategy. All aspects of the copy are the same
     * except for the roads and possibly the name.
     * 
     * @return  A copy of this strategy.
     * @throws SecurityException 
     * @throws NoSuchMethodException 
     * @throws InvocationTargetException 
     * @throws IllegalArgumentException 
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     */
    public Strategy copy() throws NoSuchMethodException, SecurityException,
            InstantiationException, IllegalAccessException,
            IllegalArgumentException, InvocationTargetException {
        Class clazz = this.getClass();
        Constructor con = clazz.getConstructor(Model.class, String.class,
                boolean.class);
        Object s = con.newInstance(this.getModel(), this.getName(),
                this.traceIsOn());
        return (Strategy) s;
    }
    
    /**
     * Return the next index of a road that is non-empty from a starting road index
     * Return -1 if all roads are clear
     * Just brute-forcing this check since there are at most 4 needed checks
     */
    protected int rotateRoads(int startIndex) {
        if (startIndex == -1) {
            startIndex = 0;
        }
    	for (int i = 0; i < 4; i++) {
    		int checkIndex = (startIndex + i)%4;
    		if (in[checkIndex] != null && in[checkIndex].getWaitingCount() > 0) {
    			return checkIndex;
    		}
    	}
    	return -1;
    }
    
    /*
     * The methods below are currently unused.
     */
    
    /* TODO: This code is very repetitive and may have some bugs. Need to trace through and clean up the code. 
     * [N, E, S, W]
     * Check to see if other cars are able to move given the pathway of the currently directed road
	 *     ||      For example, a car coming from the south direction can travel to the North, West, or East roads.
	 *   =    =    Assume it goes from South -> North. Valid options are right turn options where the entering road
	 *     ||      is not North. So, North -> West is valid.
	 *             If it were South -> East, valid ones would be East -> North; North -> South; North -> West
	 *             If it were South -> West, valid ones would be North -> East or East -> North. Only one can occur
	 *         	   The first is most likely in real life, but the second is more likely in an emergency scenario.
	 *             So, if we're turning left, check valid left turns first, then valid right turns.
	 *             If we're going straight, check valid opposite continuations first, then valid right turns.
	 *             If we're going right, check valid right turns first, then valid left turns.
	 */
    /* protected List<Road> findValidOppositeRoads(Road enteringDirection, Road oppositeDirection) {
    	int enteringIndex = 0;
    	int exitingIndex = 0;
    	for (int i = 0; i < in.length; i++) {
    		if (in[i] == enteringDireciton) {
    			enteringIndex = i;
    		}

    		if (in[i] == exitingDirection) {
    			exitingIndex = i;
    		}
    	}
    	
    	List<Road> roads = new ArrayList<Road>();
    	int turnId = turnType(enteringDirection, oppositeDirection);
    	
    	//TODO: Ensure proper handling of null roads occurs
    	//TODO: Are empty roads handled?
    	if (turnId == 1) {
    		Road direction = getDirection(oppositeDirection);
    		if (direction == enteringDirection) { //is an opposite turn
    			roads.add(enteringDirection);
    			return roads;
    		}
    		
    		direction = getDirection(in[(enteringIndex + 1) %4]);

    		if (direction == enteringDirection || direction == oppositeDirection) {
    			roads.add(in[(enteringIndex + 1) % 4]);
    			return roads;
    		}
    	} else if (turnId == 2) {//left turn
    	  Road direction = getDirection(oppositeDirection);
    	  if (turnType(oppositeDirection, direction) == 2) {
    		  roads.add(oppositeDirection);
    	  }

    	 direction = getDirection(in[(enteringIndex - 1) %4]);
    	 if (direction != oppositeDirection) {
    			roads.add(in[(enteringIndex - 1) % 4]);
    			return roads;
    	 }
    	} else if (turnId == 3) {//right turn
    		//check for other valid right turns
    		boolean foundRight = false;
    		Road direction = getDirection(oppositeDirection);
    		int turn = turnType(oppositeDirection, direction);
    		if (turn == 1) {
    			roads.add(oppositeDirection);
    			return roads;
    		} else if (turn == 3){
    			roads.add(oppositeDirection);
    			foundRight = true;
    		}
    		
    		direction = getDirection(in[(oppositeIndex - 1) %4]);
    		turn = turnType(in[(oppositeIndex -1) % 4], direction);
    		if (turn == 3) {
    			roads.add(in[(oppositeIndex -1) % 4]);
    			foundRight = true;
    			return roads;
    		} else if (turn == 1) {
    			roads.add(in[(oppositeIndex -1) % 4]);
    			return roads;
    		}

    		direction = getDirection(in[(oppositeIndex + 2) %4]);
    		turn = turnType(in[(oppositeIndex + 2) % 4], direction);
    		
    		if (turn == 2) {
    			roads.add(in[(oppositeIndex + 2) % 4]);
    			return roads;
    		}
    		
    	}
    	
    	return roads;
    }
    
    protected int turnType(Road enteringDirection, Road oppositeDirection) {
    	int enteringIndex = 0;
    	int exitingIndex = 0;

    	for (int i = 0; i < in.length; i++) {
    		if (in[i] == enteringDireciton) {
    			enteringIndex = i;
    		}

    		if (in[i] == oppositeDirection) {
    			exitingIndex = i;
    		}
    	}
    	
    	//TODO: create static constants for this
    	int id; //turn ID

    	//Determine what type of turn this is: left, right, or straight
    	if ((enteringIndex + 2) % 4 == exitingIndex) { //straight direction
    		id = 1;
    	} else if ((enteringIndex + 1) % 4 == exitingIndex) { //left turn
    		id = 2;
    	} else if ((enteringIndex + 3) % 4 == exitingIndex) { //right turn
    		id = 3;
    	} else {
    		id = -1; //not a valid turn
    	}
    }
    */
}
