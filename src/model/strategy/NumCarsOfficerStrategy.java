package model.strategy;

import model.CarTrafficEvacuationModel;
import model.Road;
import model.util.AStarSearchHeuristic;
import desmoj.core.dist.DiscreteDistEmpirical;
import desmoj.core.dist.DiscreteDistPoisson;
import desmoj.core.dist.UniformRandomGenerator;
import desmoj.core.simulator.Model;


/**
 * Officer strategy where an officer allows up to a certain number of cars from a certain direction to go on a road.
 * Once this number has been reached, the direction flow of the cars should change by moving to the right
 * 
 * directs towards the nearest exit
 * @author Lauren
 *
 */
public class NumCarsOfficerStrategy extends OfficerStrategy {

	public final static Long avg_cars_leaving = (long) 10;
	private boolean fixedTimes;
	private int currentIndex; //road currently being pulled from
    private int directionIndex;
	private int currentCarCount;
	private Long currentMax;
	private DiscreteDistPoisson numCarsDist;
	
    public NumCarsOfficerStrategy(Model owner, String name, boolean showInTrace,DiscreteDistPoisson dist) {
        super(owner, name, showInTrace);
        setCarExitDistribution(dist);
        currentIndex = -1;
    }
    
    public NumCarsOfficerStrategy(Model owner, String name, boolean showInTrace) {
        this(owner, name, showInTrace, null);
    }
    
    public Road getNext() {
    	if (currentIndex == -1 || !hasValidExits()) {
    		return null;
    	}
        return in[currentIndex];
    }
    
    @Override
    public void setExitingRoads(Road[] roads) {
        super.setExitingRoads(roads);
        this.calculateDirection();
    }
    
    /**
     * Sets the specified distribution as the distribution generating random
     * number of cars allowed to cross.
     * 
     * Should only be used when initializing the model.
     * 
     * @param dist  A numerical distribution producing int-valued numbers
     */
    public void setCarExitDistribution(DiscreteDistPoisson dist) {
        if (null != dist) {
            UniformRandomGenerator rng = ((CarTrafficEvacuationModel)
                    this.getModel()).getRNG();
            fixedTimes = false;
            dist.changeRandomGenerator(rng);
            this.numCarsDist = dist;
			currentMax = numCarsDist.sample();
        } else {
            fixedTimes = true;
            currentMax = avg_cars_leaving; 
        }
    }
    
    public void notifyInFlow(Road road) { 
    	if (currentIndex == -1 || !out[directionIndex].isFull())
    	    currentIndex = rotateRoads(0);
    }
    
    public void notifyOutFlow(Road road) {
        if (currentIndex == -1 && road == out[directionIndex])
            currentIndex = rotateRoads(0);
    }
    
    /**
     * Uses a-star search heuristic
     */
    public Road getDirection(Road road) { 
    	return in[directionIndex];
    }
    
    public boolean needsChange() {
    	return currentCarCount == 0;
    }

	@Override
	public boolean hasValidExits() {
    	return out[directionIndex].isFull();
    }
	
	@Override
	public void notifyCrossing() {
    	if (!out[directionIndex].isFull()) {
    	    currentCarCount++;
    	    if (currentCarCount >= currentMax) {
    			currentCarCount = 0;
    			if (!fixedTimes) {
    				currentMax = numCarsDist.sample();
    			}
    			currentIndex = rotateRoads(currentIndex);
    			if (currentIndex == directionIndex) {
    			    currentIndex = rotateRoads(currentIndex);
    			}
    		}
    	} else {
    	    currentCarCount = 1;
            if (!fixedTimes) {
                currentMax = numCarsDist.sample();
            }
            currentIndex = -1;
    	}
	}
	
	private void calculateDirection() {
	    AStarSearchHeuristic heuristic = ((CarTrafficEvacuationModel) this.getModel()).getHeuristic();
        int mini = 0;
        double min = 0;
	    for (int i = 0; i < 4; i++) {
            if (out[i] != null) {
                double current = heuristic.costFrom(out[i].getSource());
                double cost = heuristic.costFrom(out[i].getDestination());
                double edge = out[i].getEstimatedMean();
                double val = cost + edge;
                if (val <= current) {
                    if (0 == mini) {
                        mini = i;
                        min = current;
                    } else {
                        if (current < min) {
                            mini = i;
                            min = current;
                        }
                    }
                }
            }
	    }
	    directionIndex = mini;
	}

	private void calculateCrossing() {
	    
	}
}
