package model.strategy;

import model.CarTrafficEvacuationModel;
import model.Road;
import model.util.AStarSearchHeuristic;
import desmoj.core.dist.DiscreteDistPoisson;
import desmoj.core.dist.UniformRandomGenerator;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.SimClock;

/**
 * Officer strategy for directing traffic through an intersection. Cars on opposite roads / non-conflicting directions
 * are free to travel for a certain period of time. After this period of time elapses, the officer directs the next pair
 * of opposite / non-conflicting roads to cross the intersection.
 * 
 * @author Lauren
 *
 */
public class TimerOfficerStrategy extends OfficerStrategy {
	
	public final double avg_cars_crossing = 10.0;
	private double currentMaxTime;
	private double currentTime;
	private boolean fixedTimes;
	private int currentRoadIndex;
	private SimClock simClock;
	private DiscreteDistPoisson timeCrossingDist;

    public TimerOfficerStrategy(Model owner, String name, DiscreteDistPoisson timeCrossingDist, boolean showInTrace) {
        super(owner, name, showInTrace);
        simClock = ((CarTrafficEvacuationModel)this.getModel()).getExperiment().getSimClock();
        setCarExitDistribution(timeCrossingDist);
        currentRoadIndex = -1;
    }

	@Override
	public Road getNext() {
		if (currentRoadIndex == -1 || !hasValidExits() || getDirection(in[currentRoadIndex]) == null) {
			return null;
		}
		
		return in[currentRoadIndex];
	}

	/**
	 * Uses a* search heuristic
	 */
    public Road getDirection(Road road) { 
    	if (road == null) return null;
    	AStarSearchHeuristic heuristic = ((CarTrafficEvacuationModel)this.getModel()).getHeuristic();
    	double minCost = Integer.MAX_VALUE;
    	int minIndex = (currentRoadIndex + 1) % 4;
    	double estimatedMean = road.getEstimatedMean();
    	for (int i = 0; i < out.length; i++) {
    		if (out[i] != null && i != currentRoadIndex) {
    			double cost = heuristic.costFrom(out[i].getDestination());
    			if (cost + estimatedMean < minCost) {
    				minCost = cost;
    				minIndex = i;
    			}
    		}
    	}
    	return in[minIndex];
    }

	@Override
	public boolean hasValidExits() {
		for (int i = 0; i < out.length; i++) {
    		Road exit = out[i];
    		if (exit != null && !exit.isFull()) {
    			return true;
    		}
    	}
    	
    	return false;
    }

	@Override
	public boolean needsChange() {
        currentTime = simClock.getTime().getTimeAsDouble();
		return currentTime >= currentMaxTime;
	}

	@Override
	public void notifyInFlow(Road road) {
		if (currentRoadIndex == -1) {
			currentRoadIndex = rotateRoads(0);
		}
	}

	@Override
	public void notifyOutFlow(Road road) {}

	/* TODO: This probably isn't going to work with the timer.
	 * will probably need to implement something that calls notify crossing every
	 * so often? Or this should be the responsibility of the class using strategy?
	 * But it might work like this too. Just need to test it.
	 * 
	 * yep, calling this is the responsibility of intersection class
	 * the strategy class should not have to worry about timing related issues,
	 * time calculations are handled by intersections -Brian
	 */
	@Override
	public void notifyCrossing() {
        currentTime = simClock.getTime().getTimeAsDouble();
		if (currentTime >= currentMaxTime) {
			currentTime = simClock.getTime().getTimeAsDouble();
			if (fixedTimes) {
				currentMaxTime = currentTime + avg_cars_crossing; 
			} else {
				currentMaxTime = currentTime + timeCrossingDist.sample();
			}

			currentRoadIndex = rotateRoads(currentRoadIndex);
		}
	}
	
	 /**
     * Sets the specified distribution as the distribution generating random
     * the time cars on a road will be allowed to cross.
     * 
     * Should only be used when initializing the model.
     * 
     * @param dist  A numerical distribution producing double-valued numbers
     * 				in seconds
     */
    public void setCarExitDistribution(DiscreteDistPoisson dist) {
        currentTime = simClock.getTime().getTimeAsDouble();
        if (null != dist) {
            UniformRandomGenerator rng = ((CarTrafficEvacuationModel)
                    this.getModel()).getRNG();
            fixedTimes = false;
            dist.changeRandomGenerator(rng);
            this.timeCrossingDist = dist;
			currentMaxTime = currentTime + timeCrossingDist.sample();
        } else {
            fixedTimes = true;
            currentMaxTime = currentTime + avg_cars_crossing; 
        }
    }
}
