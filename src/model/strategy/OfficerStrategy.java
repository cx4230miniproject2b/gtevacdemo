package model.strategy;

import desmoj.core.simulator.Model;


public abstract class OfficerStrategy extends Strategy {

    public OfficerStrategy(Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
    }

}
