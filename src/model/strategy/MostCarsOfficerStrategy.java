package model.strategy;

import model.CarTrafficEvacuationModel;
import model.Location;
import model.Road;
import model.util.AStarSearchHeuristic;
import desmoj.core.simulator.Model;


/*
 * Officer strategy for directing traffic where the road with the most cars will go in an attempt to prevent roads
 * from reaching capacity.
 * If more than one road has the same number of cars, preference should be given to the currently directed road.
 */
public class MostCarsOfficerStrategy extends OfficerStrategy {

	private boolean needsChange = false;
	private int[] roadCounts = new int[4];
	private int currentInRoad; //road currently being pulled from
    public MostCarsOfficerStrategy(Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
    }
    
    public Road getNext() {
    	if (currentInRoad == -1 || !hasValidExits() || getDirection(in[currentInRoad]) == null) {
    		return null;
    	}
    	int maxRoad = 0;
        for (int i = 1; i < in.length; i++) {
        	if (in[i].getCount() > in[maxRoad].getCount()) {
        		maxRoad = i;
        	}
        }
        
        Road currentRoad = in[maxRoad];
        return currentRoad;
    }
    
    @Override
    public void setEnteringRoads(Road[] roads) {
    	super.setEnteringRoads(roads);
    	if (hasAllEmpty()) {
    		currentInRoad = -1;
    		return;
    	}
    	for (int i = 0; i < roads.length; i++) {
    		if (roads[i] != null) {
    			roadCounts[i] = roads[i].getCount();
    			if (roadCounts[currentInRoad] < roads[i].getCount()) {
    				currentInRoad = i;
    			}
    		}
    	}
    }
    
    private boolean hasAllEmpty() {
    	for (int i = 0; i < in.length; i++) {
    		if (in[i] != null && in[i].getCount() > 0) return false;
    	}
    	
    	return true;
    }
    
    public void notifyInFlow(Road road) {
    	for (int i = 0; i < in.length; i++) {
    		if (in[i] == road) {
    			roadCounts[i]++;
    			if (currentInRoad == -1 || roadCounts[i] > roadCounts[currentInRoad]) {
    				needsChange = true;
    				currentInRoad = i;
    			} else {
    				needsChange = false;
    			}

    			return;
    		}
    	}
    }
    
    public void notifyOutFlow(Road road) {
    	for (int i = 0; i < in.length; i++) {
    		if (in[i] == road) {
    			roadCounts[i]--;
    			
    			if (i == currentInRoad) {
    				for (int j = 0; j < in.length; j++) {
    					if (roadCounts[j] > roadCounts[currentInRoad]) {
    						currentInRoad = j;
    						needsChange = true;
    					} else {
    						needsChange = false;
    					}
    				}
    			}
    			
    			if (hasAllEmpty()) currentInRoad = -1;
    			return;
    		}
    	}
    }
    
    /**
     * Uses a* search heuristic to find the least cost path.
     */
    public Road getDirection(Road road) { 
    	if (road == null) return null;
    	AStarSearchHeuristic<Location, Road> heuristic = ((CarTrafficEvacuationModel)this.getModel()).getHeuristic();
    	double minCost = Integer.MAX_VALUE;
    	int minIndex = (currentInRoad + 1) % 4;
    	double estimatedMean = road.getEstimatedMean();
    	for (int i = 0; i < out.length; i++) {
    		if (out[i] != null && i != currentInRoad) {
    			double cost = heuristic.costFrom(out[i].getDestination());
    			if (cost + estimatedMean < minCost) {
    				minCost = cost;
    				minIndex = i;
    			}
    		}
    	}
    	return in[minIndex];
    }
    
    public boolean needsChange() {
    	return needsChange;
    }
    
    public boolean hasValidExits() {
    	for (int i = 0; i < out.length; i++) {
    		Road exit = out[i];
    		if (exit != null && !exit.isFull()) {
    			return true;
    		}
    	}
    	
    	return false;
    }
    
    public void notifyCrossing() {}
    
}
