package model;

import java.util.Set;

import desmoj.core.simulator.Model;


public class Exit extends Location {

    public Exit(Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
    }
    
    /**
     * Returns an empty list of roads.
     * 
     * @return  An empty list of roads.
     */
    @Override
    public Set<Road> getExitingRoads() {
        return null;
    }
    
    /**
     * Does nothing because an exit has no exiting roads.
     * 
     * @param roads An array of roads.
     */
    @Override
    public void setExitingRoads(Road[] roads) {}

    /**
     * Returns true because cars may always exit.
     * 
     * @return  True because cars may always exit.
     */
    @Override
    public boolean isAccessible() {
        return true;
    }
    
    /**
     * Does nothing because a car gets to exit upon arrival.
     * 
     * @param road  An entering road.
     */
    @Override
    public void notifyInFlow(Road road) {}

    /**
     * Does nothing because an exit has no exiting roads.
     * 
     * @param road  An exiting road.
     */
    @Override
    public void notifyOutFlow(Road road) {}

    // TODO events
}
