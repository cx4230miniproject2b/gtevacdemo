package model.util;

import java.util.Collection;

import org.jgrapht.Graph;

/**
 * Uniform heuristic is an A* search heuristic which determines if a specified
 * vertex is a goal.
 * 
 * @author Brian Dong
 *
 * @param <V>   A vertex class.
 * @param <E>   An edge class.
 */
public class UniformHeuristic<V, E> extends AStarSearchHeuristic<V, E> {
    
    /** The set of valid goal vertices. */
    private final Collection<V> goals;
    
    /**
     * Constructs a new heuristic from the specified graph and goals.
     * 
     * @param graph A graph.
     * @param goals A collection of goal vertices.
     */
    public UniformHeuristic(Graph<V, E> graph, Collection<V> goals) {
        super(graph, goals);
        this.goals = goals;
    }
    
    /**
     * Returns 0 if the specified vertex is a goal; otherwise returns 1.
     * 
     * @param vertex    A vertex.
     * @return  0 if the specified vertex is a goal, 0 otherwise.
     */
    @Override
    public double costFrom(V vertex) {
        if (goals.contains(vertex)) return 0;
        return 1;
    }

}
