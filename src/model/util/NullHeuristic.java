package model.util;

import java.util.Collection;

import org.jgrapht.Graph;

/**
 * Null heuristic is a default A* search heuristic which provides no
 * information at all.
 * 
 * @author Brian Dong
 * 
 * @param <V>   A vertex class.
 * @param <E>   An edge class.
 */
public class NullHeuristic<V, E> extends AStarSearchHeuristic<V, E> {
    

    
    /**
     * Constructs a new heuristic for the specified graph and goals.
     * 
     * This does nothing.
     * 
     * @param graph A graph network.
     * @param goals A collection of goal vertices.
     */
    NullHeuristic(Graph<V, E> graph, Collection<V> goals) {
        super(graph, goals);
    }
    
    /**
     * Constructs a new heuristic.
     * 
     * This does nothing.
     */
    NullHeuristic() {
        this(null, null);
    }

    /**
     * Returns 0.
     * 
     * @param vertex    A vertex.
     * @return 0.
     */
    @Override
    public double costFrom(V vertex) {
        return 0;
    }
}
