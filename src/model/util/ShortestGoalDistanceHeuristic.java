package model.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.jgrapht.Graph;
import org.jgrapht.alg.FloydWarshallShortestPaths;

/**
 * Shortest goal distance heuristic is an A* search heuristic which
 * is the distance to the nearest goal from a specified vertex in a graph
 * network.
 * 
 * This heuristic does not account for costs incurred at each vertex.
 * 
 * @author Brian Dong
 *
 * @param <V>   A vertex class.
 * @param <E>   An edge class.
 * @see AStarSearchHeuristic
 * @see FloydWarshallShortestPaths
 */
public class ShortestGoalDistanceHeuristic<V, E>
        extends AStarSearchHeuristic<V, E> {
    
    /** A map containing shortest goal distances. */
    private Map<V, Double> values;
    
    /**
     * Constructs a new heuristic for the specified graph and goals.
     * 
     * @param graph A graph network.
     * @param goals A collection of goal vertices.
     */
    public ShortestGoalDistanceHeuristic(Graph<V, E> graph,
            Collection<V> goals) {
        super(graph, goals);
        this.values = new HashMap<V, Double>();
        this.calculateHeuristic(graph, goals);
    }

    /**
     * Returns the distance from the specified vertex to the nearest goal.
     * 
     * This distance does not account for costs incurred at each vertex.
     * 
     * @param vertex    The source vertex.
     * @return  The distance to the nearest goal.
     */
    @Override
    public double costFrom(V vertex) {
        return values.get(vertex);
    }
    
    /**
     * Calculates the heuristic values by computing all the shortest paths
     * between all vertices using the Floyd-Warshall shortest paths algorithm,
     * and then storing the minimum distance between all vertices and all goal
     * vertices.
     * 
     * @param graph The network to calculate heuristics for.
     * @param goals A set of goal vertices in the graph network.
     * @see FloydWarshallShortestPaths
     */
    private void calculateHeuristic(Graph<V, E> graph, Collection<V> goals) {
        FloydWarshallShortestPaths<V, E> fw =
                new FloydWarshallShortestPaths<V, E>(graph);
        Collection<V> vertices = graph.vertexSet();
        Collection<V> invalids = new HashSet<V>();
        for (V goal : goals) {
            if (!vertices.contains(goal)) {
                invalids.add(goal);
            }
        }
        goals.remove(invalids);
        for (V source : vertices) {
            for (V goal : goals) {
                double val = fw.shortestDistance(source, goal);
                if (values.keySet().contains(source)) {
                    values.put(source, Math.min(val, values.get(source)));
                } else {
                    values.put(source,  val);
                }
            }
        }
    }
}
