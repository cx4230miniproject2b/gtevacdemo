package model.util;

import desmoj.core.dist.MersenneTwisterRandomGenerator;
import desmoj.core.dist.UniformRandomGenerator;

import org.apache.commons.math3.distribution.ChiSquaredDistribution;

/**
 * Driver class for statistically testing the MersenneTwisterRandomGenerator as a PRNG
 * Uses a Chi-Square test for the PRNG. 
 * ChiSquaredDistribution is used to calculate the p-value.
 */
public class PRNGTest {
	
	private static final double significance_level = .01;
	private static final int sampleNumber = 1000; //number of samples to take
	private static final int sampleSize = 900000; //size of each sample

	//Compare bit strings
	public static void main(String[] args) {
		UniformRandomGenerator rng = new MersenneTwisterRandomGenerator();

		//Create a new ChiSquaredDistribution with 1 degree of freedom
		ChiSquaredDistribution csd = new ChiSquaredDistribution(1);
		
		//Running count of the number of statistically significat samples that appear to not be random.
		int statSig = 0;
		int[] v = new int[2];
		v[0] = (int) (sampleSize/2f);
		v[1] = (int) (sampleSize/2f);
	    	
		for (int i = 0; i < sampleNumber; i++) {
			double[] y = new double[2];
			for (int j = 0; j < sampleSize; j++) {
				int bit = 0;
				boolean  b = rng.nextDouble() < .5;
				if (b) {
					bit = 1;
				}

				y[bit] = y[bit] + 1;
			}

			double[] x = new double[2];
			x[0] = ((double)(Math.pow(y[0] - v[0], 2))) / (double)v[0];
			x[1] = ((double)(Math.pow(y[1] - v[1], 2))) / (double)v[1];
			double chiSquare = x[0] + x[1];

			/* Use p-value calculator to calculate the correct value and compare with a significance level of .01
			 * to determine how randomly the PRNG generates numbers. P-values less than the significance value mean it's 
			 * unlikely the sequence is random so we want larger p-values.
			 */
			double pvalue = csd.cumulativeProbability(chiSquare);

			if (pvalue < significance_level) {
				statSig++;
			}
		}

		double statSigPercent = ((double)statSig) / ((double)sampleNumber);
		System.out.println(statSigPercent + "% of samples are statistically significant. (" + statSig + " out of " + sampleNumber + ")");
	}
}
