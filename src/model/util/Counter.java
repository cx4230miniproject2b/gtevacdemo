package model.util;

/**
 * A counter that can be incremented or decremented. If a counter has a
 * limit, then it can no longer be incremented after reaching its limit.
 * 
 * @author Brian Dong
 */
public interface Counter {
    
    /**
     * Returns the current count.
     * 
     * @return  The number of items being counted.
     */
    public int getCount();
    
    /**
     * Returns whether this counter has reached its maximum.
     * 
     * @return  True if this counter has reached its maximum, false otherwise.
     */
    public boolean isFull();
    
    /**
     * Increments this counter. Returns true if successful, otherwise returns
     * false.
     * 
     * @return  True if successful, false otherwise.
     */
    public boolean increment();
    
    /**
     * Decrements this counter. Returns true if successful, otherwise returns
     * false.
     * 
     * @return  True if successful, false otherwise.
     */
    public boolean decrement();
    
}