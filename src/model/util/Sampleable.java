package model.util;

/**
 * A sampleable object which provides some numerical sample from its own
 * numerical distribution.
 * 
 * @author Brian Dong
 *
 * @param <N>   A number class.
 */
public interface Sampleable<N extends Number> {
    
    /**
     * Returns the estimated mean of this object's numerical distribution.
     * 
     * @return  The estimated mean of this object's numerical distribution.
     */
    public N getEstimatedMean();
    
    /**
     * Returns a numerical sample of type N from this object's numerical
     * distribution.
     * 
     * @return  A numerical sample of type N from this object's numerical
     *          distribution.
     */
    public N sample();
}
