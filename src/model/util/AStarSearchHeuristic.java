package model.util;

import java.util.Collection;

import org.jgrapht.Graph;

/**
 * A* search heuristic is a projected cost heuristic which estimates the 
 * distance to the nearest goal from a specified vertex in a graph network.
 * 
 * @author Brian Dong
 *
 * @param <V>   A vertex class.
 * @param <E>   An edge class.
 */
public abstract class AStarSearchHeuristic<V, E> {
    
    /**
     * Constructs a new heuristic for the specified graph and goals.
     * 
     * This does nothing, and only exists to enforce the minimum required
     * constructor inputs.
     * 
     * @param graph A graph network.
     * @param goals A collection of goal vertices.
     */
    AStarSearchHeuristic(Graph<V, E> graph, Collection<V> goals) {}
    
    /**
     * Returns an estimate of the distance to nearest goal from the specified
     * vertex.
     * 
     * @param vertex    A vertex.
     * @return  An estimate of the distance to the nearest goal from the
     *          specified vertex.
     */
    public abstract double costFrom(V vertex);

}
