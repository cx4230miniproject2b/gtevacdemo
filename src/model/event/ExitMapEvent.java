package model.event;

import model.CarTrafficEvacuationModel;
import model.Exit;
import model.Road;
import desmoj.core.simulator.Event;
import desmoj.core.simulator.EventOf2Entities;
import desmoj.core.simulator.Model;

/**
 * An event where a car exits the simulation by traveling to an exit at the
 * edge of the simulation's map.
 * 
 * @author Brian Dong
 * @see CarTrafficEvacuationModel#exitMap(Road, Exit)
 */
public class ExitMapEvent extends EventOf2Entities<Road, Exit> {
    
    /**
     * Constructs a new exit map event from the specified inputs.
     * 
     * @param owner         The model this event belongs to.
     * @param name          The string name of this event.
     * @param showInTrace   A boolean determining traceability.
     * @see ExitMapEvent
     * @see Event#Event(Model, String, boolean)
     */
    public ExitMapEvent(Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
    }
    
    /**
     * A car exits the simulation.
     * 
     * @param road          The road a car is currently traveling on.
     * @param destination   The exit a car is arriving at.
     * @see ExitMapEvent
     */
    @Override
    public void eventRoutine(Road source, Exit destination) {
        CarTrafficEvacuationModel model =
                (CarTrafficEvacuationModel) this.getModel();
        // DEBUG
        //System.out.println(this.presentTime() + ", " + this.getName());
        model.exitMap(source, destination);
    }
}
