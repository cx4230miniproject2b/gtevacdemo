package model.event;

import model.CarTrafficEvacuationModel;
import model.Intersection;
import model.Road;
import desmoj.core.simulator.Event;
import desmoj.core.simulator.EventOf3Entities;
import desmoj.core.simulator.Model;

/**
 * An event where a car finishes crossing an intersection and begins to
 * travel down a road.
 * 
 * @author Brian Dong
 * @see CarTrafficEvacuationModel#travel(Road, Intersection, Road)
 */
public class TravelingEvent extends EventOf3Entities<Road, Intersection, Road> {
    
    /**
     * Construct a new traveling event from the specified inputs.
     * 
     * @param owner         The model this event belongs to.
     * @param name          The string name of this event.
     * @param showInTrace   A boolean determining traceability.
     * @see TravelingEvent
     * @see Event#Event(Model, String, boolean)
     */
    public TravelingEvent(Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
    }
    
    /**
     * A car finishes crossing an intersection and begins to travel down a
     * road.
     * 
     * @param source        The road a car is exiting.
     * @param intersection  The intersection a car is crossing.
     * @param destination   The road a car is entering.
     * @see TravelingEvent
     */
    @Override
    public void eventRoutine(Road source, Intersection intersection,
            Road destination) {
        CarTrafficEvacuationModel model =
                (CarTrafficEvacuationModel) this.getModel();
        // DEBUG
        //System.out.println(this.presentTime() + ", " + this.getName());
        model.travel(source, intersection, destination);
    }
}
