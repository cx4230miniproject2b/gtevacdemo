package model.event;

import model.CarTrafficEvacuationModel;
import model.ParkingLot;
import model.Road;
import desmoj.core.simulator.Event;
import desmoj.core.simulator.EventOf2Entities;
import desmoj.core.simulator.Model;

/**
 * An event where a car exits a parking lot and enters a road.
 * 
 * @author Brian Dong
 * @see CarTrafficEvacuationModel#exitLot(ParkingLot, Road)
 */
public class ExitLotEvent extends EventOf2Entities<ParkingLot, Road> {
    
    /**
     * Construct a new exit lot event from the specified inputs.
     * 
     * @param owner         The model this event belongs to.
     * @param name          The string name of this event.
     * @param showInTrace   A boolean determining traceability.
     * @see ExitLotEvent
     * @see Event#Event(Model, String, boolean)
     */
    public ExitLotEvent(Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
    }
    
    /**
     * A car exits a parking lot and enters a road.
     * 
     * @param source        The parking lot a car is exiting.
     * @param destination   The road a car is entering.
     * @see ExitLotEvent
     */
    @Override
    public void eventRoutine(ParkingLot source, Road destination) {
        CarTrafficEvacuationModel model =
                (CarTrafficEvacuationModel) this.getModel();
        // DEBUG
        //System.out.println(this.presentTime() + ", " + this.getName());
        model.exitLot(source, destination);
    }
}
