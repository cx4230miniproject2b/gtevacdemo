package model.event;

import model.CarTrafficEvacuationModel;
import model.Road;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Event;
import desmoj.core.simulator.Model;

/**
 * An event where a car arrives at the end of the road it is currently
 * traveling on.
 * 
 * @author Brian Dong
 * @see CarTrafficEvacuationModel#arrival(Road)
 */
public class ArrivalEvent extends Event<Road> {
    
    /**
     * Constructs a new arrival event from the specified inputs.
     * 
     * @param owner         The model this event belongs to.
     * @param name          The string name of this event.
     * @param showInTrace   A boolean determining traceability.
     * @see ArrivalEvent
     * @see Event#Event(Model, String, boolean)
     */
    public ArrivalEvent(Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
    }
    
    /**
     * A car arrives at the end of the specified road.
     * 
     * @param road  The road a car is currently traveling on.
     * @see ArrivalEvent
     */
    @Override
    public void eventRoutine(Road road) {
        CarTrafficEvacuationModel model =
                (CarTrafficEvacuationModel) this.getModel();
        // DEBUG
        //System.out.println(this.presentTime() + ", " + this.getName());
        model.arrival(road);
    }
}
