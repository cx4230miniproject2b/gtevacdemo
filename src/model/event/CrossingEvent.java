package model.event;

import model.CarTrafficEvacuationModel;
import model.Intersection;
import desmoj.core.simulator.Event;
import desmoj.core.simulator.Model;

/**
 * An event where a car begins crossing an intersection.
 * 
 * @author Brian Dong
 * @see CarTrafficEvacuationModel#cross(Intersection)
 */
public class CrossingEvent extends Event<Intersection> {
    
    /**
     * Construct a new crossing event from the specified inputs.
     * 
     * @param owner         The model this event belongs to.
     * @param name          The string name of this event.
     * @param showInTrace   A boolean determining traceability.
     * @see CrossingEvent
     * @see Event#Event(Model, String, boolean)
     */
    public CrossingEvent(Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
    }
    
    /**
     * A car begins crossing an the specified intersection.
     * 
     * @param intersection  The intersection to cross.
     * @see CrossingEvent
     */
    @Override
    public void eventRoutine(Intersection intersection) {
        CarTrafficEvacuationModel model =
                (CarTrafficEvacuationModel) this.getModel();
        // DEBUG
        //System.out.println(this.presentTime() + ", " + this.getName());
        model.cross(intersection);
    }
}
