package model;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import model.strategy.IntersectionStrategy;
import model.strategy.Strategy;
import model.util.Sampleable;
import desmoj.core.dist.NumericalDist;
import desmoj.core.dist.UniformRandomGenerator;
import desmoj.core.simulator.Model;

// TODO class description
public class Intersection extends Location implements Sampleable<Double> {
    
    public static final String DEFAULT_STRATEGY_NAME =
            IntersectionStrategy.class.getName();
    /** The current estimated mean. */
    private double estimate;
    /** The number of samples generated. */
    private int numSamples;
    /** The amount of time needed to wait for a car on a different road to
     *  completely cross. */
    private double changeTime;
    /** The strategy determining which car moves next in the
     *  intersection. */
    private Strategy strategy;
    /** Whether the crossing times are fixed. */
    private boolean fixedTimes;
    /** If the crossing times are fixed, then the time it takes to cross */
    private double crossingTime;
    /** If the crossing times are not fixed, then a distribution of crossing
     *  times. */
    private NumericalDist<Double> crossingTimeDistribution;
    /** Whether a crossing is currently scheduled for this intersection. */
    private boolean active;

    // TODO javadoc constructors
    public Intersection(Model owner, String name, boolean showInTrace,
            double changeTime, Strategy strategy, double crossingTime,
            NumericalDist<Double> dist) {
        super(owner, name, showInTrace);
        this.estimate = 0; // TODO estimate mean
        this.numSamples = 0;
        this.changeTime = changeTime < 0 ?
                CarTrafficEvacuationModel.DEFAULT_CHANGE_TIME : changeTime;
        this.setStrategy(strategy);
        this.crossingTime = crossingTime < 0 ?
                CarTrafficEvacuationModel.DEFAULT_CROSSING_TIME : crossingTime;
        this.setCrossingTimeDistribution(dist);
    }
    
    public Intersection(Model owner, String name, boolean showInTrace,
            double changeTime, Strategy strategy, double crossingTime) {
        this(owner, name, showInTrace, changeTime, strategy, crossingTime,
                null);
    }
    
    public Intersection(Model owner, String name, boolean showInTrace,
            double changeTime, Strategy strategy, NumericalDist<Double> dist) {
        this(owner, name, showInTrace, changeTime, strategy, -1, dist);
    }
    
    public Intersection(Model owner, String name, boolean showInTrace,
            double crossingTime) {
        this(owner, name, showInTrace, -1, null, crossingTime, null);
    }
    
    public Intersection(Model owner, String name, boolean showInTrace,
            NumericalDist<Double> dist) {
        this(owner, name, showInTrace, -1, null, -1, dist);
    }
    
    public Intersection(Model owner, String name, boolean showInTrace) {
        this(owner, name, showInTrace, -1, null, -1, null);
    }
    
    /**
     * Returns an estimate of the mean time in seconds for the next car to
     * cross.
     * 
     * @return  An estimate of the mean time for the next car to cross.
     */
    @Override
    public Double getEstimatedMean() {
        // TODO update so that its a rolling estimated that is updated
        //      whenever sample() is called
        return this.crossingTime;
    }
    
    /**
     * Returns the time in seconds for the next car to cross.
     * 
     * @return  The time for the next car to cross.
     */
    @Override
    public Double sample() {
        Double sample = (Double) (this.fixedTimes ?
                this.crossingTime : this.crossingTimeDistribution.sample());
        return sample + (this.strategy.needsChange() ? this.changeTime : 0);
    }
    
    @Override
    public void setEnteringRoads(Road[] roads) {
        super.setEnteringRoads(roads);
        this.strategy.setEnteringRoads(roads);
    }
    
    @Override
    public void setExitingRoads(Road[] roads) {
        super.setExitingRoads(roads);
        this.strategy.setExitingRoads(roads);
    }
    
    /**
     * Returns whether this intersection does not have a crossing in progress.
     * 
     * @return  True if this intersection does not have a crossing in progress,
     *          false otherwise.
     */
    @Override
    public boolean isAccessible() {
        return !this.active && this.strategy.hasValidExits();
    }
    
    /**
     * Updates this intersection's strategy based on potential in flow.
     * 
     * @param road  An entering road.
     * @see Strategy#notifyInFlow(Road)
     */
    @Override
    public void notifyInFlow(Road road) {
        this.strategy.notifyInFlow(road);
    }
    
    /**
     * Updates this intersection's strategy based on potential out flow.
     * 
     * @param road  An exiting road.
     * @see Strategy#notifyOutFlow(Road)
     */
    @Override
    public void notifyOutFlow(Road road) {
        this.strategy.notifyOutFlow(road);
    }
    
    /**
     * Sets the specified distribution as this intersection's new crossing time
     * distribution if possible; otherwise sets the crossing time distribution
     * to a fixed time.
     * 
     * Should only be used when initializing the model.
     * Recommended to make the distribution's random number generator the same
     * as the model's random number generator beforehand.
     * 
     * @param dist  The distribution to set as this intersection's new
     *              crossing time distribution.
     */
    public void setCrossingTimeDistribution(NumericalDist<Double> dist) {
        if (null != dist) {
            this.fixedTimes = false;
            this.crossingTimeDistribution = dist;
        } else {
            this.fixedTimes = true;
        }
    }
    
    // TODO need a method to set a strategy?
    
    /**
     * Returns the next road whose car may cross during the next crossing.
     * 
     * @return  The next road whose car may cross during the next crossing.
     * @see Strategy#getNext()
     */
    public Road getNext() {
        return this.strategy.getNext();
    }
    
    /**
     * Returns the intended direction of travel for the next car on the
     * specified road.
     * 
     * @param road  A entering road.
     * @return  An exiting road which is the intended direction of travel for
     *          the next car to cross.
     * @see Strategy#getDirection(Road)
     */
    public Road getDirection(Road road) {
        return this.strategy.getDirection(road);
    }
    
    /**
     * Returns the class of this intersection's strategy.
     * 
     * @return  The class of this intersection's strategy.
     */
    public Class<? extends Strategy> getStrategyClass() {
        return this.strategy.getClass();
    }
    
    public String getCrossString() {
        Road source = this.strategy.getNext();
        // TODO can change getDirection to not take inputs since only one crossing now
        // TODO could possibly even return direction ids instead of roads themselves
        Road destination = this.strategy.getDirection(source);
        int s = 0;
        int d = 0;
        for (int i = 0; i < 4; i++) {
            if (this.in[i] == source) {
                s = i;
            }
            if (this.out[i] == destination) {
                d = i;
            }
        }
        String str = this.directionIdToString(s) + this.directionIdToString(d);
        return str;
    }
    
    /**
     * Sets this intersection as having a crossing in progress.
     */
    public void setActive() {
        this.active = true;
    }
    
    /**
     * Set this intersection's strategy to a copy of the specified strategy.
     * 
     * @param strategy  An intersection strategy.
     */
    public void setStrategy(Strategy strategy) {
        try {
            if (null == strategy) {
                this.strategy = this.defaultStrategy();
                this.strategy.setEnteringRoads(this.in);
                this.strategy.setExitingRoads(this.out);
            } else {
                this.strategy = strategy.copy();
                this.strategy.setEnteringRoads(this.in);
                this.strategy.setExitingRoads(this.out);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Returns whether this intersection has an officer directing traffic.
     * 
     * @return  True if this intersection has an officer directing traffic,
     *          false otherwise.
     */
    public boolean hasOfficer() {
        return this.strategy.hasOfficer();
    }
    
    /**
     * Allows the next car to cross the intersection.
     */
    public void cross() {
        this.strategy.notifyCrossing();
        this.active = false;
    }
    
    /**
     * Returns a default intersection strategy.
     * 
     * @return  A default intersection strategy.
     * @see IntersectionStrategy
     */
    private Strategy defaultStrategy() {
        return new IntersectionStrategy(this.getModel(), this.getName()
                + "'s Strategy", this.traceIsOn());
    }
    
    /**
     * Converts the specified direction id into a string.
     * 
     * @param directionId   A direciton id.
     * @return  A string representing a direction.
     */
    private String directionIdToString(int directionId) {
        String s = null;
        switch (directionId) {
        case Location.NORTH:
            s = "N";
            break;
        case Location.EAST:
            s = "E";
            break;
        case Location.SOUTH:
            s = "S";
            break;
        case Location.WEST:
            s = "W";
            break;
        }
        return s;
    }
}