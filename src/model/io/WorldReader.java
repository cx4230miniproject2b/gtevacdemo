package model.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.io.File;

import model.Intersection;

/**
 * Reads the world1.csv file to get information about the 
 * streets and parking lots in the world.
 * @author Lauren Winston
 *
 */
public class WorldReader implements InputReader {
	
	/**
	 * HashSet containing the string names of all parking lots.
	 */
	private HashSet<String> parkingNames;

	/**
	 * HashSet containing all streets, including roads and 
	 * roads from parking lots.
	 */
	private HashSet<String> streetNames;

	/**
	 * HashSet containing the locations of all intersections.
	 */
	private HashSet<String> intersections;

	/**
	 * HashSet containing the locations of all exits from
	 * the world.
	 */
	private HashSet<String> exitLocs;
	
	/**
	 * HashSet containing the locations of all parking lots.
	 * Note: Each parking lot has two associated locations, a 
	 * start and an end. This stores the starting location.
	 */
	private HashSet<String> parkingEntranceLocs;
	
	/**
	 * Mapping from the string name of a street to its 
	 * capacity. This includes both parking lots and non-
	 * parking-lot roads.
	 */
	private HashMap<String, String> nameCapacityMap;
	
	/**
	 * Mapping from the string name of a street to its
	 * length.
	 */
	private HashMap<String, String> nameLengthMap;

	/**
	 * Mapping from the string name of a street to a 
	 * HashSet of its start and end locations.
	 * Note: Each HashSet should have two values.
	 */
	private HashMap<String, HashSet<String>> roadNameToAdjacentLocationsMapping;

	/**
	 * Mapping from a string location to the street names that
	 * pass through this location.
	 */
	private HashMap<String, List<String>> locationToSourceMap;
	
	/**
	 * Mapping from a location where a parking lot is, to the 
	 * name of the parking lot at that location.
	 */
	private HashMap<String, String> parkingLocToNameMap;

	/*
	 * Used for debugging and testing
	 * Can be removed in final portion of project
	 */
	public static void main(String[] args) {
		WorldReader reader = new WorldReader();
		String[] testSortLocs = reader.getRoads("(441,84)");
		for (int i = 0; i < testSortLocs.length; i++) {
			if (testSortLocs[i] != null) {
				System.out.println(testSortLocs[i]);
			} else {
				System.out.println(i + " is null");
			}
		}
	}
	
	/*
	 * Reads in csv file and populates hash sets with the data.
	 * TODO: Remove print lines once debugging is complete
	 */
	public WorldReader() {
		nameCapacityMap = new HashMap<String, String>();
		roadNameToAdjacentLocationsMapping = new HashMap<String, HashSet<String>>();
		nameLengthMap = new HashMap<String, String>();
		parkingEntranceLocs = new HashSet<String>();
		locationToSourceMap = new HashMap<String, List<String>>();
		parkingLocToNameMap = new HashMap<String, String>();
		parkingNames = new HashSet<String>();
		streetNames = new HashSet<String>();
		intersections = new HashSet<String>();
		exitLocs = new HashSet<String>();
		String dir = System.getProperty("user.dir");
		File csvFile = new File(dir + "/world1.csv");

		try {
			Scanner scanner = new Scanner(csvFile);
			scanner.useDelimiter(",|\n");
			scanner.nextLine();

			//used to prevent duplicate values from being added after other duplicates already found
			HashSet<String> removed = new HashSet<String>(); 
			while (scanner.hasNext()) {
				String[] currentStreetRep = new String[7];
				for (int i = 0; i < 7; i++) {
					currentStreetRep[i] = scanner.next().replaceAll("\\r\\n|\\r|\\n", " ");
				}
				
				//The current actual name for a road. Ex. "Cherry"
				String currentName = currentStreetRep[6];

				//The starting location of the street
				String start = "(" + currentStreetRep[1] + "," + currentStreetRep[2] + ")";
				
				//The ending location of the street
				String end = "(" + currentStreetRep[3] + "," + currentStreetRep[4] + ")";

				//If it's a parking lot, populate the relevant collections.
				if (currentStreetRep[0].equals("Parking")) {
					parkingNames.add(currentName);
					parkingEntranceLocs.add(start);
					parkingLocToNameMap.put(start, currentName);
				}
					
				streetNames.add(currentName);
					
				String length = getDistance(currentStreetRep[1], currentStreetRep[2],currentStreetRep[3], currentStreetRep[4]);
				nameLengthMap.put(currentName, length);
					
				//Determine if it's an intersection or not.
				if (!intersections.contains(start)) {
					intersections.add(start);
				}

				if (!intersections.contains(end)) {
					intersections.add(end);
				}
					
				//Creates name to location of intersection mappings.
				HashSet<String> collection = new HashSet<String>();
				collection.add(start);
				collection.add(end); 

				roadNameToAdjacentLocationsMapping.put(currentName, collection);

				//Population the hashset of valid exits.
				if (exitLocs.contains(start)) {
					exitLocs.remove(start);
					removed.add(start);
				} else if (!removed.contains(start)) {
					exitLocs.add(start);
				}
					
				if (exitLocs.contains(end)) {
					exitLocs.remove(end);
					removed.add(end);
				} else if (!removed.contains(end)) {
					exitLocs.add(end);
				}

				List<String> currList;
				if (!locationToSourceMap.containsKey(start)) {
					currList = new ArrayList<String>();
				} else {
					currList = locationToSourceMap.get(start);
				}
				
				currList.add(currentName);
				locationToSourceMap.put(start, currList);
				
				if (!locationToSourceMap.containsKey(end)) {
					currList = new ArrayList<String>();
				} else {
					currList = locationToSourceMap.get(end);
				}
				
				currList.add(currentName);
				locationToSourceMap.put(end, currList);
				
				//populate relevant hashmaps
				nameCapacityMap.put(currentName, currentStreetRep[5]);
			}
			
			scanner.close();
			
			Iterator<String> i = intersections.iterator();
			while (i.hasNext()) {
				String loc = (String)i.next();
				List<String> adjacent = locationToSourceMap.get(loc);
				if (adjacent == null || adjacent.size() <= 1) {
					i.remove();
				}
			}
		} catch (Exception e) {
			System.out.println("Invalid file name.");
		}
	}
	
	/**
	 * @return a hash set of all the valid parking lot names
	 */
	public HashSet<String> getAllParkingLotNames() {
		return parkingNames;
	}
	
	/**
	 * @return a hash set of all the valid parking lot names
	 */
	public HashSet<String> getAllParkingLotLocations() {
		return parkingEntranceLocs;
	}
	
	/**
	 * @return a hash set of all the valid street names
	 */
	public HashSet<String> getAllRoadNames() {
		return streetNames;
	}
	
	/**
	 * A street an exit if (x2, y2) != any other 
	 * street's (x2, y2)
	 * 
	 * @return A hashset of all the valid exit names
	 */
	public HashSet<String> getAllExitNames() {
	    // DEBUG
	    // there are too many exits...
	    for (String name : exitLocs) {
	        System.out.println("exit: " + name);
	    }
		return exitLocs;
	}
	
	/**
	 * @param name The name of the street or the parking lot.
	 * @return The capacity of the street or parking lot.
	 */
	public int getCapacity(String name) {
		return Integer.parseInt(((String)nameCapacityMap.get(name)));
	}

	/**
	 * Optional implementation
	 * 
	 * @return whether intersection has officer.
	 */
    public boolean hasOfficer(String name) {
        // TODO Auto-generated method stub
        return false;
    }

    /*
     * return all intersection coordinates
     * it is an intersection unless it is a parking lot or an exit
     */
    public HashSet<String> getAllIntersectionNames() {
        // DEBUG checking for overlap
        for (String name : intersections) {
            if (parkingEntranceLocs.contains(name)) {
                System.out.println("parking overlap: " + name);
            }
            if (exitLocs.contains(name)) {
                System.out.println("exit overlap: " + name);
            }
        }
    	return intersections;
    }

    /*
     * should return length defined as distance between x1,y1 and x2,y2
     * converted into miles by the map scale. then multiply by the #of lanes
     * in the road, which is just the road capacity
     */
    public double getLength(String name) {
        return Double.parseDouble(nameLengthMap.get(name));
    }

    /*
     * should return the two endpoints of a road
     */
    public String[] getEndsOfRoad(String name) {
    	Object[] set = roadNameToAdjacentLocationsMapping.get(name).toArray();
    	return Arrays.copyOf(set, set.length, String[].class);
    }
    
    /**
     * Note: it's assumed that location is a valid 
     * parking lot location. 
     * @param location String location of a parking lot.
     * @return The string name of the parking lot.
     */
    public String getParkingLotName(String location) {
    	return parkingLocToNameMap.get(location);
    }

    /*
     * should return the names of the streets at a location as an array
     * [N, E, S, W]
     * @param name A location name
     */
    public String[] getRoads(String name) {
    	String[] roads = new String[4];
    	List<String> locations;

    	//attempting to access with a name instead of location
    	if (parkingNames.contains(name)) {
    		HashSet<String> nameSet = roadNameToAdjacentLocationsMapping.get(name);
    		Iterator<String> iterator = nameSet.iterator();
    		String loc1 = iterator.next();
    		if (locationToSourceMap.get(loc1) == null) {
    			locations = locationToSourceMap.get(iterator.next());
    		} else {
    			locations = locationToSourceMap.get(loc1);
    		}
    	} else {
    		locations = locationToSourceMap.get(name);
    	}

    	if (locations == null) {
    		for (int i = 0; i < roads.length; i++) {
    			roads[i] = null;
    		}
    	} else if (locations.size() > 4) { 
    		System.out.println("Error: too many locations returned"); 
    	} else {
    		roads = sortLocs(locations, name);
    	}

    	return roads;
    }
    
    //assume parameters are valid strings 
    private String getDistance(String startX, String startY, String endX, String endY){
    	int xStart = Integer.parseInt(startX);
    	int yStart = Integer.parseInt(startY);
    	int xEnd = Integer.parseInt(endX);
    	int yEnd = Integer.parseInt(endY);
    
    	return "" + Math.sqrt(Math.pow(xStart - xEnd, 2) + Math.pow(yStart - yEnd, 2));
    }
    
    private int[] parseLocation(String location) {
    	location = location.substring(1, location.length() - 1);
    	
    	int[] intArr = new int[2];
    	for (int i = 0; i<location.length(); i++) {
    		if (location.charAt(i) == ',') {
    			intArr[0] = Integer.parseInt(location.substring(0, i));
    			intArr[1] = Integer.parseInt(location.substring(i+1));
    			break;
    		}
    	}
    	return intArr;
    }
    
    /**
     * Sort the locations into the cardinal direction NESW
     * Note: This algorithm won't correctly return the cardinal directions in all cases but
     * will work for the purposes of this simulation. When the roads have very similar coordinates,
     * the correct roads won't always be placed in the correct cardinal position, but as the
     * roads in this simulation are significantly different from another, this should not be an
     * issue.
     * @param List of string roads that pass through the location.
     * @param locationString The string location we're looking at. 
     * @return
     */
    private String[] sortLocs(List<String> list, String locationString) {
    	//Translate the string location into array form.
    	int[] intersection = parseLocation(locationString);
    	
    	//Initialize the array that will be returned.
    	String[] adjacentRoadNames = new String[4];
    	for (int i = 0; i < adjacentRoadNames.length; i++) {
    		adjacentRoadNames[i] = null;
    	}

    	int[] one = null;
    	int[] two = null;
    	int[] three = null;
    	int[] four = null;
    	
    	String oneStr = null;
    	String twoStr = null;
    	String threeStr = null;
    	String fourStr = null;

    	Iterator<String> adjacentRoadListIterator = list.iterator();
    	int count = 0;
    	while (adjacentRoadListIterator.hasNext()) {
    		String currAdjacentRoad = adjacentRoadListIterator.next();
    		
    		//Get the set of end points for the current adjacent road.
    		HashSet<String> currAdjacentRoadEndsSet = roadNameToAdjacentLocationsMapping.get(currAdjacentRoad);
    		
    		//Iterator to get the two endpoints of current road.
    		Iterator<String> endsSetIterator = currAdjacentRoadEndsSet.iterator();
    		
    		//An adjacent road has two locations, one that overlaps with the intersection.
    		//Find the location that doesn't overlap with the intersection.
    		String currString = endsSetIterator.next();
    		int[] uniqueEndpoint = new int[2];

    		if (currString.equals(locationString)) {
    			uniqueEndpoint = parseLocation(endsSetIterator.next());
    		} else {
    			uniqueEndpoint = parseLocation(currString);
    		}
    		
    		//Store the endpoint values and the name of the road with that endpoint
    		//for each adjacent road.
    		if (count == 0) {one = uniqueEndpoint; oneStr = currAdjacentRoad;}
    		if (count == 1) {two = uniqueEndpoint; twoStr = currAdjacentRoad;}
    		if (count == 2) {three = uniqueEndpoint; threeStr = currAdjacentRoad;}
    		if (count == 3) {four = uniqueEndpoint; fourStr = currAdjacentRoad;}
    		
    		count++;
    	}
    	
    	if (one != null) {
    		adjacentRoadNames[findDirectionIndex(one, intersection)] = oneStr;
    	}
    	if (two != null) {
    		int startIndex = findDirectionIndex(two, intersection);

    		//If the first two adjacent locations pointed to the same cardinal direction
    		//Find the nearest open location.
    		while (adjacentRoadNames[startIndex] != null) {
    			startIndex = (startIndex + 1)%4;
    		}
    		
    		adjacentRoadNames[startIndex] = twoStr;
    	}
    	if (three != null) {
    		int startIndex = findDirectionIndex(three, intersection);

    		//If the suggested index is shared with another direction,
    		//Find the nearest open location.
    		while (adjacentRoadNames[startIndex] != null) {
    			startIndex = (startIndex + 1)%4; 
    		}
    		
    		adjacentRoadNames[startIndex] = threeStr;
    	}
    	if (four != null) {
    		int startIndex = findDirectionIndex(four, intersection);

    		//Find the nearest open location if the suggested index 
    		//is shared with another adjacent road.
    		while (adjacentRoadNames[startIndex] != null) {
    			startIndex = (startIndex + 1)%4;
    		}
    		
    		adjacentRoadNames[startIndex] = fourStr;
    	}
    	
    	return adjacentRoadNames;
    }
    
    /**
     * Given that roads are stored as [N E W S], calculate and return the
     * index of the cardinal direction that most closely represent the
     * pair of coordinates.
     * Together, both parameters form the two endpoints of a road. 
     * @param road Unique coordinates of a road.
     * @param intersection Intersection coordinates.
     * @return Index of cardinal direction.
     */
    private int findDirectionIndex(int[] road, int[] intersection) {
    	int xDiff = Math.abs(road[0] - intersection[0]);
    	int yDiff = Math.abs(road[1] - intersection[1]);
    	
    	if (xDiff >= yDiff && intersection[0] >= road[0]) { return 3; }
    	if (xDiff >= yDiff && intersection[0] <= road[0]) { return 1; }
    	if (xDiff <= yDiff && intersection[1] >= road[1]) { return 2; }
    	if (xDiff <= yDiff && intersection[1] <= road[1]) { return 0; }
    	
    	return 0;
    }


}
