package model.io;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.StatisticalSummary;

import model.data.Results;

public class BasicCSVReport extends ResultsReport {
    
    public BasicCSVReport(Results results) {
        super(results);
    }
    
    @Override
    public String content() {
        String content = "";
        content += this.processExits() + "\n\n";
        content += this.processParkingLots() + "\n\n";
        content += this.processIntersections() + "\n";
        return content;
    }
    
    @Override
    public String fileType() {
        return ".csv";
    }
    
    private String processParkingLots() {
        String content = "Parking Lots";
        NumberFormat nf = NumberFormat.getPercentInstance();
        for (String lot : results.getParkingLotNames()) {
            content += "\n" + lot + "," + this.results.getInitialCount(lot)
                    + "," + nf.format(this.results.getInitialPercent(lot));
        }
        return content;
    }
    
    private String processIntersections() {
        String content = "Intersections";
        for (String intersection : this.results.getIntersectionNames()) {
            content += "\n" + intersection + ","
                    + this.results.getStrategy(intersection);
        }
        return content;
    }
    
    private String processExits() {
        String content = "Exits";
        NumberFormat nf = new DecimalFormat("#0.00");
        NumberFormat nf2 = new DecimalFormat("#0.####");
        DescriptiveStatistics diffs = new DescriptiveStatistics();
        String body = "";
        boolean first = true;
        double sum = 0;
        double prev = 0;
        for (String exit : this.results.getExitNames()) {
            body += "\n" + exit;
            for (double time : this.results.getExitTimes(exit)) {
                body += "," + nf.format(time);
                sum += time;
                if (!first) {
                    diffs.addValue(time - prev);
                } else {
                    first = false;
                }
                prev = time;
            }
        }
        content += "\nsum,mean difference,standard deviation difference";
        content += "\n" + nf.format(sum) + "," + nf2.format(diffs.getMean())
                + "," + nf2.format(diffs.getStandardDeviation());
        content += body;
        return content;
    }
}
