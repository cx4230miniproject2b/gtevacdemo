package model.io;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


public class CSVReader implements InputReader {
    
    public static final double conversion = 0.094697/70; //miles per logical unit
    
    private Map<String, String> lots; // coor: name
    private Set<String> intersections; // coor
    private Set<String> exits; // coor
    private final Map<String, String[]> roads; // road: coors
    private Map<String, String[]> locations; // coor: roads
    private final Map<String, Integer> capacities; // name: capacity
    private final Map<String, Double> lengths; // road: length
    
    
    public CSVReader(String path, String fileName) {
        if (null == path) {
            path = System.getProperty("user.dir");
        }
        this.lots = new HashMap<String, String>();
        this.intersections = new HashSet<String>();
        this.exits = new HashSet<String>();
        this.roads = new HashMap<String, String[]>();
        this.locations = new HashMap<String, String[]>();
        this.capacities = new HashMap<String, Integer>();
        this.lengths = new HashMap<String, Double>();
        path = this.correctPath(path);
        File file = new File(path + fileName + ".csv");
        this.processFile(file);
        //this.fixNames();
    }
    
    public CSVReader(String fileName) {
        this(null, fileName);
    }
    
    @Override
    public Collection<String> getAllParkingLotLocations() {
        return this.lots.keySet();
    }
    
    @Override
    public Collection<String> getAllIntersectionNames() {
        return this.intersections;
    }
    
    @Override
    public Collection<String> getAllExitNames() {
        return this.exits;
    }
    
    @Override
    public Collection<String> getAllRoadNames() {
        return this.roads.keySet();
    }
    
    @Override
    public String getParkingLotName(String lotCoor) {
        return lots.get(lotCoor);
    }
    
    @Override
    public int getCapacity(String lotName) {
        return this.capacities.get(lotName);
    }
    
    @Override
    public boolean hasOfficer(String intersectionCoor) {
        return false;
    }
    
    @Override
    public double getLength(String road) {
        return this.lengths.get(road);
    }
    
    @Override
    public String[] getEndsOfRoad(String road) {
        return this.roads.get(road);
    }
    
    @Override
    public String[] getRoads(String locationCoor) {
        return this.locations.get(locationCoor);
    }
    
    private void processFile(File file) {
        try {
            Scanner scanner = new Scanner(file);
            scanner.useDelimiter(",|\n");
            scanner.nextLine();
            Map<String, Collection<String>> coorToRoads =
                    new HashMap<String, Collection<String>>();
            Map<String, String[]> lotNameToCoors =
                    new HashMap<String, String[]>();
            while (scanner.hasNext()) {
                String[] line = new String[7];
                for (int i = 0; i < 7; i++) {
                    line[i] = scanner.next().replaceAll("\\r\\n|\\r|\\n", " ");
                }
                String name = line[6];
                String coor1 = "(" + line[1] + "," + line[2] + ")";
                String coor2 = "(" + line[3] + "," + line[4] + ")";
                int cap = Integer.parseInt(line[5]);
                double length = this.calcDistance(line[1], line[2], line[3],
                        line[4]);
                switch (line[0]) {
                case "Street":
                    this.processStreetLine(name, coor1, coor2, length,
                            coorToRoads);
                    break;
                case "Parking":
                    this.processParkingLine(name, coor1, coor2, cap, length,
                            coorToRoads, lotNameToCoors);
                    break;
                }
            }
            scanner.close();
            processInputs(coorToRoads, lotNameToCoors);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /*
     * For reference
    private final set<String> intersections; // coor: officer
    private final Set<String> exits; // coor
     */
    
    private void processInputs(Map<String, Collection<String>> coorToRoads,
            Map<String, String[]> lotNameToCoors) {
        // Sort roads for each location
        for (String coor : coorToRoads.keySet()) {
            String[] sorted = sortLocs(coorToRoads.get(coor), coor);
            this.locations.put(coor, sorted);
        }
        // Find parking lot locations
        for (String lot : lotNameToCoors.keySet()) {
            String[] coors = lotNameToCoors.get(lot);
            if (1 == coorToRoads.get(coors[0]).size()) {
                this.lots.put(coors[0], lot);
            } else if (1 == coorToRoads.get(coors[1]).size()) {
                this.lots.put(coors[1], lot);
            }
        }
        // Find intersections and exits
        for (String coor : this.locations.keySet()) {
            if (!lots.containsKey(coor)) {
                if (1 < coorToRoads.get(coor).size()) {
                    this.intersections.add(coor);
                } else {
                    this.exits.add(coor);
                }
            }
        }
    }
    
    private void processStreetLine(String road, String coor1, String coor2,
            double length, Map<String, Collection<String>> coorToRoads) {
        this.addRoad(road, coor1, coor2, length, coorToRoads);
    }
    
    private void processParkingLine(String lot, String coor1, String coor2,
            int capacity, double length, Map<String, Collection<String>> coorToRoads,
            Map<String, String[]> lotNameToCoors) {
        String road = "Road to " + lot;
        this.addRoad(road, coor1, coor2, length, coorToRoads);
        this.addCapacity(lot, capacity);
        lotNameToCoors.put(lot, new String[]{coor1, coor2});
    }
    
    private void addRoad(String road, String coor1, String coor2,
            double length, Map<String, Collection<String>> coorToRoads) {
        this.addRoadToCoor(coor1, road, coorToRoads);
        this.addRoadToCoor(coor2, road, coorToRoads);
        this.addLength(road, length);
        this.roads.put(road, new String[]{coor1, coor2});
    }
    
    private void addRoadToCoor(String coor, String road,
            Map<String, Collection<String>> coorToRoads) {
        if (!coorToRoads.containsKey(coor)) {
            coorToRoads.put(coor, new LinkedList<String>());
        }
        coorToRoads.get(coor).add(road);
    }
    
    private void addCapacity(String lot, int capacity) {
        this.capacities.put(lot, capacity);
    }
    
    private void addLength(String road, double length) {
        this.lengths.put(road, length);
    }
    
    private double calcDistance(String x1s, String y1s, String x2s,
            String y2s) {
        int x1 = Integer.parseInt(x1s);
        int y1 = Integer.parseInt(y1s);
        int x2 = Integer.parseInt(x2s);
        int y2 = Integer.parseInt(y2s);
        int xdiff = x1 - x2;
        int ydiff = y1 - y2;
        return Math.sqrt(xdiff*xdiff + ydiff*ydiff)*conversion;
    }
    
    private String correctPath(String path) {
        if (path.length() - 1 != path.lastIndexOf(File.separator)) {
            path += File.separator;
        }
        return path;
    }
    
    private void fixNames() {
        String begin = "";
        String end = "fix";
        Map<String, String> lots2 = new HashMap<String, String>(); // coor: name
        Set<String> intersections2 = new HashSet<String>(); // coor
        Set<String> exits2 = new HashSet<String>(); // coor
        Map<String, String[]> locations2 = new HashMap<String, String[]>(); // coor: roads
        for (String coor : lots.keySet()) {
            String newCoor = begin + coor + end;
            lots2.put(newCoor, lots.get(coor));
        }
        lots = lots2;
        for (String coor : intersections) {
            String newCoor = begin + coor + end;
            intersections2.add(newCoor);
        }
        intersections = intersections2;
        for (String coor : exits) {
            String newCoor = begin + coor + end;
            exits2.add(newCoor);
        }
        exits = exits2;
        for (String road : roads.keySet()) {
            String[] arr = roads.get(road);
            arr[0] = begin + arr[0] + end;
            arr[1] = begin + arr[1] + end;
        }
        for (String coor : locations.keySet()) {
            String newCoor = begin + coor + end;
            locations2.put(newCoor, locations.get(coor));
        }
        locations = locations2;
    }
    
    // untested below, by Lauren Winston
    
    /**
     * Sort the locations into the cardinal direction NESW
     * Note: This algorithm won't correctly return the cardinal directions in all cases but
     * will work for the purposes of this simulation.
     * 
     * +x seems to be east
     * +y seems to be south
     * @param coll
     * @param locationString
     * @return
     */
    private String[] sortLocs(Collection<String> roads, String location) {
        int[] loc = this.parseLocation(location);
        String[] directions = new String[]{null, null, null, null};
        Map<String, int[]> coors = new HashMap<String, int[]>();
        for (String road : roads) {
            String[] ends = this.roads.get(road);
            if (location.equals(ends[0])) {
                coors.put(road, this.parseLocation(ends[1]));
            } else {
                coors.put(road, this.parseLocation(ends[0]));
            }
        }
        Map<String, Integer> xdiffs = new HashMap<String, Integer>();
        Map<String, Integer> ydiffs = new HashMap<String, Integer>();
        for (String road : coors.keySet()) {
            int[] coor = coors.get(road);
            int xdiff = coor[0] - loc[0];
            int ydiff = coor[1] - loc[1];
            xdiffs.put(road, xdiff);
            ydiffs.put(road, ydiff);
        }
        Set<String> assigned = new HashSet<String>();
        String north = null;
        String east = null;
        String south = null;
        String west = null;
        boolean n = false;
        boolean e = false;
        boolean s = false;
        boolean w = false;
        int size = roads.size();
        int count = 0;
        while (assigned.size() < size && count++ < 5) {
            north = null;
            east = null;
            south = null;
            west = null;
            for (String road : coors.keySet()) {
                if (!assigned.contains(road)) {
                    int xdiff = xdiffs.get(road);
                    int ydiff = ydiffs.get(road);
                    if (!n) {
                        if (null == north) {
                            if (ydiff < 0)
                                north = road;
                            if (4 == count)
                                north = road;
                        } else if (ydiff < ydiffs.get(north)) {
                            north = road;
                        }
                    }
                    if (!s) {
                        if (null == south) {
                            if (ydiff > 0)
                                south = road;
                            if (4 == count)
                                south = road;
                        } else if (ydiff > ydiffs.get(south)) {
                            south = road;
                        }
                    }
                    if (!e) {
                        if (null == east) {
                            if (xdiff > 0)
                                east = road;
                            if (4 == count)
                                east = road;
                        } else if (xdiff > xdiffs.get(east)) {
                            east = road;
                        }
                    }
                    if (!w) {
                        if (null == west) {
                            if (xdiff < 0)
                                west = road;
                            if (4 == count)
                                west = road;
                        } else if (xdiff < xdiffs.get(west)) {
                            west = road;
                        }
                    }
                }
            }
            int xdist = 0;
            int ydist = 0;
            if (null != north && !assigned.contains(north) && !n) {
                if (north == east || north == west) {
                    xdist = Math.abs(xdiffs.get(north));
                    ydist = Math.abs(ydiffs.get(north));
                    if (ydist > xdist) {
                        directions[0] = north;
                        assigned.add(north);
                        north = null;
                        n = true;
                    }
                } else {
                    directions[0] = north;
                    assigned.add(north);
                    north = null;
                    n = true;
                }
            }
            if (null != south && !assigned.contains(south) && !s) {
                if (south == east || south == west) {
                    xdist = Math.abs(xdiffs.get(south));
                    ydist = Math.abs(ydiffs.get(south));
                    if (ydist > xdist) {
                        directions[2] = south;
                        assigned.add(south);
                        south = null;
                        s = true;
                    }
                } else {
                    directions[2] = south;
                    assigned.add(south);
                    south = null;
                    s = true;
                }
            }
            if (null != east && !assigned.contains(east) && !e) {
                if (east == north || east == south) {
                    xdist = Math.abs(xdiffs.get(east));
                    ydist = Math.abs(ydiffs.get(east));
                    if (xdist > ydist) {
                        directions[1] = east;
                        assigned.add(east);
                        east = null;
                        e = true;
                    }
                } else {
                    directions[1] = east;
                    assigned.add(east);
                    east = null;
                    e = true;
                }
            }
            if (null != west && !assigned.contains(west) && !w) {
                if (west == north || west == south) {
                    xdist = Math.abs(xdiffs.get(west));
                    ydist = Math.abs(ydiffs.get(west));
                    if (xdist > ydist) {
                        directions[3] = west;
                        assigned.add(west);
                        west = null;
                        w = true;
                    }
                } else {
                    directions[3] = west;
                    assigned.add(west);
                    west = null;
                    w = true;
                }
            }
            /*
            if (count == 4) {
                if (n) {
                    String r = directions[0];
                    System.out.println("N: " + r);
                    System.out.println(xdiffs.get(r));
                    System.out.println(ydiffs.get(r));
                }
                if (e) {
                    String r = directions[1];
                    System.out.println("E: " + r);
                    System.out.println(xdiffs.get(r));
                    System.out.println(ydiffs.get(r));
                }
                if (s) {
                    String r = directions[2];
                    System.out.println("S: " + r);
                    System.out.println(xdiffs.get(r));
                    System.out.println(ydiffs.get(r));
                }
                if (w) {
                    String r = directions[3];
                    System.out.println("W: " + r);
                    System.out.println(xdiffs.get(r));
                    System.out.println(ydiffs.get(r));
                }
                count++;
                for (String r : roads) {
                    if (!assigned.contains(r)) {
                        System.out.println("unassigned: " + r);
                        System.out.println(xdiffs.get(r));
                        System.out.println(ydiffs.get(r));
                        
                    }
                }
            }
            */
        }
        /*
        for (int i = 0; i < 4; i++) {
            if (null == directions[i]) {
                switch (i) {
                case 0:
                    for (String road : roads) {
                        String min = null;
                        if (assigned.contains(road)) {
                            if (null == min) {
                                min = road;
                            } else {
                                
                            }
                        }
                    }
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                }
            }
        }
        */
        /*
        int[] one = null;
        int[] two = null;
        int[] three = null;
        int[] four = null;
        
        String oneStr = null;
        String twoStr = null;
        String threeStr = null;
        String fourStr = null;
        Iterator<String> i = roads.iterator();
        int count = 0;
        while (i.hasNext()) {
            String road = i.next();
            String[] ends = this.roads.get(road);
            String end1 = ends[0];
            String end2 = ends[1];
            int[] temp = new int[2];
            if (end1.equals(location)) {
                temp = parseLocation(end2);
            } else {
                temp = parseLocation(end1);
            }
                
            if (count == 0) {one = temp; oneStr = road;}
            if (count == 1) {two = temp; twoStr = road;}
            if (count == 2) {three = temp; threeStr = road;}
            if (count == 3) {four = temp; fourStr = road;}
                
            count++;
            // DEBUG
            System.out.println(count);
            if (one != null) {
                directions[findDirectionIndex(one, intersection)] = oneStr;
            }
            if (two != null) {
                int startIndex = findDirectionIndex(two, intersection);
    
                while (directions[startIndex] != null) {
                    startIndex = (startIndex + 1)%4;
                }
                
                directions[startIndex] = twoStr;
            }
            if (three != null) {
                int startIndex = findDirectionIndex(three, intersection);
    
                while (directions[startIndex] != null) {
                    startIndex = (startIndex + 1)%4; 
                }
                
                directions[startIndex] = threeStr;
            }
            if (four != null) {
                int startIndex = findDirectionIndex(four, intersection);
    
                while (directions[startIndex] != null) {
                    startIndex = (startIndex + 1)%4;
                }
                
                directions[startIndex] = fourStr;
            }
        }
        */
        return directions;
    }
    
    private int findDirectionIndex(int[] road, int[] intersection) {
        int xDiff = Math.abs(road[0] - intersection[0]);
        int yDiff = Math.abs(road[1] - intersection[1]);
        
        if (xDiff >= yDiff && intersection[0] >= road[0]) { return 3; }
        if (xDiff >= yDiff && intersection[0] <= road[0]) { return 1; }
        if (xDiff <= yDiff && intersection[1] >= road[1]) { return 2; }
        if (xDiff <= yDiff && intersection[1] <= road[1]) { return 0; }
        
        return 0;
    }
    
    private int[] parseLocation(String location) {
        location = location.substring(1, location.length() - 1);
        
        int[] intArr = new int[2];
        for (int i = 0; i<location.length(); i++) {
            if (location.charAt(i) == ',') {
                intArr[0] = Integer.parseInt(location.substring(0, i));
                intArr[1] = Integer.parseInt(location.substring(i+1));
                break;
            }
        }
        return intArr;
    }
}
