package model.io;

import java.util.Collection;

public interface InputReader {
    
    public Collection<String> getAllParkingLotLocations();
    
    public Collection<String> getAllIntersectionNames();
    
    public Collection<String> getAllExitNames();
    
    public Collection<String> getAllRoadNames();
    
    public String getParkingLotName(String parkingLotCoordinates);
    
    public int getCapacity(String parkingLotName);
    
    public boolean hasOfficer(String intersectionCoor);
    
    public double getLength(String roadName);
    
    public String[] getEndsOfRoad(String roadName);
    
    public String[] getRoads(String locationCoordinates);
}
