package model.io;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Constructor;
import java.util.Collection;

import org.jgrapht.Graph;

import model.data.Results;

public class ReportWriter {
    
    private static final String def = System.getProperty("user.dir");
    private final ResultsReport report;
    private final String path;
    private final String fileName;
    
    public ReportWriter(ResultsReport report, String path, String fileName) {
        this.report = report;
        this.path = this.correctPath(path);
        this.fileName = fileName;
    }
    
    public ReportWriter(ResultsReport report, String fileName) {
        this(report, def, fileName);
    }
    
    public ReportWriter(Results results, String path, String fileName,
            Class<? extends ResultsReport> clazz) {
        this.report = this.constructReport(results, clazz);
        this.path = this.correctPath(path);
        this.fileName = fileName;
    }
    
    public ReportWriter(Results results, String fileName,
            Class<? extends ResultsReport> clazz) {
        this(results, def, fileName, clazz);
    }
    
    public void write() {
        try {
            FileWriter writer = new FileWriter(
                    path + fileName + report.fileType());
            writer.append(report.content());
            writer.flush();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private String correctPath(String path) {
        if (path.length() - 1 != path.lastIndexOf(File.separator)) {
            path += File.separator;
        }
        return path;
    }
    
    private ResultsReport constructReport(Results results,
            Class<? extends ResultsReport> clazz) {
        Constructor con = null;
        Object r = null;
        try {
            con = clazz.getConstructor(Results.class);
            r = con.newInstance(results);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (ResultsReport) r;
    }
}
