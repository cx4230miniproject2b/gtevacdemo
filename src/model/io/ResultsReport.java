package model.io;

import model.data.Results;

public abstract class ResultsReport {
    
    public static final Class BASIC_CSV = BasicCSVReport.class;
    
    protected final Results results;
    
    public ResultsReport(Results results) {
        this.results = results;
    }
    
    /**
     * Returns the content of the report.
     * 
     * @return  The content of the report.
     */
    public abstract String content();
    
    /**
     * Returns the file type of the report.
     * 
     * @return  The file type of the report.
     */
    public abstract String fileType();
    
    /*
    these can be used to ease the process of formatting a .txt report (copied from Brian's previous work)
    public String toStringz() {
        if (!report.equals("")) {
            return report;
        }
        String sep = "\n";
        report += leftMargin + "Account Listing Report for " + fullName + sep;
        report += leftMargin + "as of " + longToDateString(date) + sep;
        @SuppressWarnings("unchecked")
        List<String>[] columns = (List<String>[]) new List<?>[2];
        columns[0] = names;
        columns[1] = amounts;
        report += listColumnsToBody(columns, new int[] {nameMax, amountMax});
        return report;
    }
    
    private void processAccounts(List<Account> accounts) {
        names = new LinkedList<String>();
        amounts = new LinkedList<String>();
        nameMax = 0;
        amountMax = 0;
        String s = null;
        for (Account a : accounts) {
            s = a.getName();
            names.add(s);
            nameMax = s.length() > nameMax ? s.length() : nameMax;
            s = floatToString(a.getBalance());
            amounts.add(s);
            amountMax = s.length() > amountMax ? s.length() : amountMax;
        }
        nameMax += -nameMax % 4;
        amountMax += -amountMax % 4;
    }
    
    protected final String listColumnsToBody(final List<String>[] columns,
            final int[] columnWidths) {
        @SuppressWarnings("unchecked")
        Iterator<String>[] iterators =
                (Iterator<String>[]) new Iterator<?>[columns.length];
        for (int i = columns.length - 1; i >= 0; i--) {
            iterators[i] = columns[i].iterator();
        }
        StringBuilder s = new StringBuilder();
        while (iterators[0].hasNext()) {
            s.append(String.format(leftMargin + "%-" + columnWidths[0] + "s",
                    iterators[0].next()));
            for (int i = 1; i < columns.length; i++) {
                s.append(String.format(buffer + "%" + columnWidths[i] + "s",
                        iterators[i].next()));
            }
            s.append(sep);
        }
        return s.toString();
    }
    */
}
