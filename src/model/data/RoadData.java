package model.data;

import java.util.LinkedList;
import java.util.List;

public class RoadData {
    
    private final List<Double> entries;
    private final List<Double> arrivals;
    
    public RoadData() {
        this.entries = new LinkedList<Double>();
        this.arrivals = new LinkedList<Double>();
    }
    
    public List<Double> getEntryTimes() {
        return this.entries;
    }
    
    public List<Double> getArrivalTimes() {
        return this.arrivals;
    }
    
    public void addEntry(double time) {
        this.entries.add(time);
    }
    
    public void addArrival(double time) {
        this.arrivals.add(time);
    }
}
