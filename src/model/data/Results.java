package model.data;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Intersection;
import model.strategy.IntersectionStrategy;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.SimClock;

public class Results {
    
    private final Map<String, ParkingLotData> lots;
    private final Map<String, IntersectionData> intersections;
    private final Map<String, ExitData> exits;
    private final Map<String, RoadData> roads;
    private SimClock simClock;
    
    public Results(Model model) {
        this.lots = new HashMap<String, ParkingLotData>();
        this.intersections = new HashMap<String, IntersectionData>();
        this.exits = new HashMap<String, ExitData>();
        this.roads = new HashMap<String, RoadData>();
        this.simClock = model.getExperiment().getSimClock();
    }
    
    // TODO all of this
    
    public Collection<String> getParkingLotNames() {
        return this.lots.keySet();
    }
    
    public Collection<String> getIntersectionNames() {
        return this.intersections.keySet();
    }
    
    public Collection<String> getExitNames() {
        return this.exits.keySet();
    }
    
    public Collection<String> getAllRoadNames() {
        return this.roads.keySet();
    }
    
    //capture initial capacities
    public double getInitialCount(String parkingLot) {
        return this.lots.get(parkingLot).getInitialCount();
    }
    
    public double getInitialPercent(String parkingLot) {
        return this.lots.get(parkingLot).getInitialPercent();
    }
    
    public List<Double> getParkingLotExitTimes(String parkingLot) {
        return this.lots.get(parkingLot).getTimes();
    }
    
    public String getStrategy(String intersection) {
        return this.intersections.get(intersection).getStrategy();
    }
    
    //capture times a car crosses an intersection
    public List<Double> getIntersectionCrossingTimes(String intersection) {
        return this.intersections.get(intersection).getTimes();
    }
    
    //capture as "NS", "ES" i.e. north to south, east to south, etc.
    public List<String> getIntersectionCrossingDirections(
            String intersection) {
        return this.intersections.get(intersection).getCrossStrings();
    }
    
    //capture exit information
    public List<Double> getExitTimes(String exit) {
        return this.exits.get(exit).getTimes();
    }
    
    public List<String> getExitSources(String exit) {
        return this.exits.get(exit).getSources();
    }
    
    //capture times a car enters a road
    public List<Double> getRoadEntryTimes(String road) {
        return this.roads.get(road).getEntryTimes();
    }
    
    public List<Double> getRoadArrivalTimes(String road) {
        return this.roads.get(road).getArrivalTimes();
    }
    
    public void setInitialLotCounts(String parkingLot, int capacity,
            double percent) {
        this.lots.put(parkingLot, new ParkingLotData(capacity, percent));
    }
    
    public void setStrategy(String intersection, String strategy) {
        this.intersections.put(intersection, new IntersectionData(strategy));
    }
    
    public void notifyExitLot(String parkingLot) {
        double time = this.getSimTime();
        this.lots.get(parkingLot).addTime(time);
        //System.out.println("pop from: " + parkingLot + " at " + time);
    }
    
    public void notifyCrossing(String intersection, String crossString) {
        if (!this.intersections.containsKey(intersection)) {
            this.intersections.put(intersection, new IntersectionData(
                    Intersection.DEFAULT_STRATEGY_NAME));
        }
        double time = this.getSimTime();
        this.intersections.get(intersection).addCrossing(time,
                crossString);
        //System.out.println("crossing at: " + intersection + " with "
        //        + crossString + " at " + time);
    }
    
    public void notifyExitMap(String exit, String source) {
        if (!this.exits.containsKey(exit)) {
            this.exits.put(exit, new ExitData());
        }
        double time = this.getSimTime();
        this.exits.get(exit).addExit(time, source);
        //System.out.println("exit at: " + exit + " from " + source
        //        + " at " + time);
    }
    
    public void notifyTravel(String road) {
        if (!this.roads.containsKey(road)) {
            this.roads.put(road, new RoadData());
        }
        double time = this.getSimTime();
        this.roads.get(road).addEntry(time);
        //System.out.println("travel on: " + road + " at " + time);
    }
    
    public void notifyArrival(String road) {
        if (!this.roads.containsKey(road)) {
            this.roads.put(road, new RoadData());
        }
        double time = this.getSimTime();
        this.roads.get(road).addArrival(time);
        //System.out.println("arrival on: " + road + " at " + time);
    }
    
    private double getSimTime() {
        return this.simClock.getTime().getTimeAsDouble();
    }
}
