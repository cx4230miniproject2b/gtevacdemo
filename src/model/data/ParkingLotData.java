package model.data;

import java.util.LinkedList;
import java.util.List;

public class ParkingLotData {
    
    private final int count;
    private final double percent;
    private final List<Double> times;
    
    public ParkingLotData(int capacity, double percent) {
        this.count = (int) Math.round(capacity*percent);
        this.percent = percent;
        this.times = new LinkedList<Double>();
    }
    
    public int getInitialCount() {
        return this.count;
    }
    
    public double getInitialPercent() {
        return this.percent;
    }
    
    public List<Double> getTimes() {
        return this.times;
    }
    
    public void addTime(double time) {
        this.times.add(time);
    }
}
