package model.data;

import java.util.LinkedList;
import java.util.List;

public class ExitData {
    
    private final List<Double> times;
    private final List<String> sources;
    
    public ExitData() {
        this.times = new LinkedList<Double>();
        this.sources = new LinkedList<String>();
    }
    
    public List<Double> getTimes() {
        return this.times;
    }
    
    public List<String> getSources() {
        return this.sources;
    }
    
    public void addExit(double time, String source) {
        this.times.add(time);
        this.sources.add(source);
    }
}
