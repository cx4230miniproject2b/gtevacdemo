package model.data;

import java.util.LinkedList;
import java.util.List;

public class IntersectionData {
    
    private final String strategy;
    private final List<Double> times;
    private final List<String> crossings;
    
    public IntersectionData(String strategy) {
        this.times = new LinkedList<Double>();
        this.crossings = new LinkedList<String>();
        this.strategy = strategy;
    }
    
    public String getStrategy() {
        return this.strategy;
    }
    
    public List<Double> getTimes() {
        return this.times;
    }
    
    public List<String> getCrossStrings() {
        return this.crossings;
    }
    
    public void addCrossing(double time, String crossString) {
        this.crossings.add(crossString);
    }
}
