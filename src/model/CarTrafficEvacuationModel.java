package model;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.jgrapht.DirectedGraph;
import org.jgrapht.EdgeFactory;
import org.jgrapht.WeightedGraph;

import model.data.Results;
import model.event.ArrivalEvent;
import model.event.CrossingEvent;
import model.event.ExitLotEvent;
import model.event.ExitMapEvent;
import model.event.TravelingEvent;
import model.io.InputReader;
import model.io.WorldReader;
import model.strategy.Strategy;
import model.util.AStarSearchHeuristic;
import model.util.UniformHeuristic;
import desmoj.core.dist.MersenneTwisterRandomGenerator;
import desmoj.core.dist.NumericalDist;
import desmoj.core.dist.UniformRandomGenerator;
import desmoj.core.simulator.Model;
import desmoj.core.simulator.TimeSpan;

// TODO javadoc
public class CarTrafficEvacuationModel extends Model
implements DirectedGraph<Location, Road>, WeightedGraph<Location, Road> {
    
    
    public static final int DEFAULT_CAPACITY = 300;
    public static final int DEFAULT_EXIT_TIME = 5;
    
    public static final int DEFAULT_CHANGE_TIME = 4;
    public static final int DEFAULT_CROSSING_TIME = 4;
    
    /** The assumed length of a car. */
    public static final double DEFAULT_ROAD_LENGTH = 0.0589; // 311 feet converted to miles
    public static final double DEFAULT_CAR_LENGTH = 0.00256; // 13.517 feet converted to miles
    public static final int DEFAULT_SPEED_LIMIT = 35;
    public static final int DEFAULT_SPEED = DEFAULT_SPEED_LIMIT;
    
    private final Set<Location> locations;
    private final Set<ParkingLot> lots;
    private final Set<Intersection> intersections;
    private final Set<Exit> exits;
    private final Set<Road> roads;
    /** The master psuedo random number generator for this model. */
    private final UniformRandomGenerator rng;
    private final InputReader info;
    private Results results;
    private AStarSearchHeuristic<Location, Road> heuristic;

    // speed in mph, lengths in miles, time in seconds
    
    /**
     * Constructs a new car traffic evacuation model from the specified inputs.
     * 
     * Must set a heuristic after connecting an experiment to this model.
     * 
     * @param owner         The supermodel this model is a submodel of.
     * @param name          The string name of this model.
     * @param showInReport  Whether this model is reportable.
     * @param showInTrace   Whether this model is traceable.
     * @param rng           A uniform random number generator.
     * @param info          An input reader which contains information about
     *                      how to construct and initialize the simulation.
     */
    public CarTrafficEvacuationModel(Model owner, String name,
            boolean showInReport, boolean showInTrace,
            UniformRandomGenerator rng, InputReader info) {
        super(owner, name, showInReport, showInTrace);
        this.locations = new HashSet<Location>();
        this.lots = new HashSet<ParkingLot>();
        this.intersections = new HashSet<Intersection>();
        this.exits = new HashSet<Exit>();
        this.roads = new HashSet<Road>();
        this.rng = rng;
        this.info = info;
        this.results = null;
        this.heuristic = null;
    }
    
    @Override
    public String description() {
        // TODO similar to readme?
        return null;
    }
    
    /**
     * Generates an exit lot event for each parking lot on each outgoing road.
     */
    @Override
    public void doInitialSchedules() {
        for (ParkingLot lot : lots) {
            for (Road road : lot.getExitingRoads()) {
                if (lot.getCount() >= 0 && !road.isFull()) {
                    this.generateExitLotEvent(lot, road);
                }
            }
        }
    }
    
    @Override
    public void init() {
        this.constructWorld();
        this.results = new Results(this);
        this.fillLots();
    }
    
    /**
     * Returns the set of roads between the specified locations.
     * 
     * @param sourceVertex  A source location.
     * @param targetVertex  A destination location.
     * @return  The set of roads between the specified locations.
     */
    @Override
    public Set<Road> getAllEdges(Location sourceVertex,
            Location targetVertex) {
        Set<Road> set = new HashSet<Road>();
        for (Road road : sourceVertex.getExitingRoads()) {
            if (road.getDestination().equals(targetVertex)) set.add(road);
        }
        return set;
    }
    
    /**
     * Returns a road between the specified locations if it exists; otherwise
     * returns null.
     * 
     * @param sourceVertex  A source location.
     * @param targetVertex  A destination location.
     * @return  The road between the specified locations if it exists, null
     *          otherwise.
     */
    @Override
    public Road getEdge(Location sourceVertex, Location targetVertex) {
        for (Road road : sourceVertex.getExitingRoads()) {
            if (road.getDestination().equals(targetVertex)) return road;
        }
        return null;
    }
    
    /**
     * Does nothing because this operation is not defined.
     * 
     * @return  Null because this operation is not defined.
     */
    @Override
    public EdgeFactory<Location, Road> getEdgeFactory() {
        return null;
    }
    
    /**
     * Does nothing because this operation is not defined.
     * 
     * @param sourceVertex  A source location.
     * @param targetVertex  A destination location.
     * @return  Null because this operation is not defined.
     */
    @Override
    public Road addEdge(Location sourceVertex, Location targetVertex) {
        return null;
    }
    
    /**
     * Does nothing because this operation is not defined.
     * 
     * @param sourceVertex  A source location.
     * @param targetVertex  A destination location.
     * @return  False because this operation is not defined.
     */
    @Override
    public boolean addEdge(Location sourceVertex, Location targetVertex,
            Road e) {
        return false;
    }
    
    /**
     * Does nothing because this operation is not defined.
     * 
     * @param v A location.
     * @return  False because this operation is not defined.
     */
    @Override
    public boolean addVertex(Location v) {
        return false;
    }
    
    /**
     * Returns whether a road exists between the specified locations.
     * 
     * @param sourceVertex  A source location.
     * @param targetVertex  A destination locatin.
     * @return  True if a road exists between the specified locations, false
     *          otherwise. 
     */
    @Override
    public boolean containsEdge(Location sourceVertex, Location targetVertex) {
        for (Road road : sourceVertex.getExitingRoads()) {
            if (road.getDestination().equals(targetVertex)) return true;
        }
        return false;
    }
    
    /**
     * Returns whether this model contains the specified road.
     * 
     * @param e A road.
     * @return  True if this model contains the specified location, false
     *          otherwise.
     */
    @Override
    public boolean containsEdge(Road e) {
        return this.roads.contains(e);
    }
    
    /**
     * Returns whether this model contains the specified location.
     * 
     * @param v A location.
     * @return  True if this model contains the specified location, false
     *          otherwise.
     */
    @Override
    public boolean containsVertex(Location v) {
        return this.locations.contains(v);
    }
    
    /**
     * Returns this model's set of roads.
     * 
     * @return  This model's set of roads.
     */
    @Override
    public Set<Road> edgeSet() {
        return this.roads;
    }
    
    /**
     * Returns a set of roads entering or exiting the specified location.
     * 
     * @param vertex    A location.
     * @return  A set of roads entering or exiting the specified location.
     */
    @Override
    public Set<Road> edgesOf(Location vertex) {
        Set set = vertex.getEnteringRoads();
        set.addAll(vertex.getExitingRoads());
        return set;
    }
    
    /**
     * Does nothing because this operation is not defined.
     * 
     * @param edges A collection of roads.
     * @return  False because this operation is not defined.
     */
    @Override
    public boolean removeAllEdges(Collection<? extends Road> edges) {
        return false;
    }
    
    /**
     * Does nothing because this operation is not defined.
     * 
     * @param sourceVertex  A source location.
     * @param targetVertex  A destination location.
     * @return  Null because this operation is not defined.
     */
    @Override
    public Set<Road> removeAllEdges(Location sourceVertex,
            Location targetVertex) {
        return null;
    }
    
    /**
     * Does nothing because this operation is not defined.
     * 
     * @param vertices  A collection of locations.
     * @return False because this operation is not defined.
     */
    @Override
    public boolean removeAllVertices(Collection<? extends Location> vertices) {
        return false;
    }
    
    /**
     * Does nothing because this operation is not defined.
     * 
     * @param sourceVertex  A source location.
     * @param targetVertex  A destination location.
     * @return  Null because this operation is not defined.
     */
    @Override
    public Road removeEdge(Location sourceVertex, Location targetVertex) {
        return null;
    }
    
    /**
     * Does nothing because this operation is not defined.
     * 
     * @param e A road.
     * @return  False because this operation is not defined.
     */
    @Override
    public boolean removeEdge(Road e) {
        return false;
    }
    
    /**
     * Does nothing because this operation is not defined.
     * 
     * @param v A location.
     * @return  False because this operation is not defined.
     */
    @Override
    public boolean removeVertex(Location v) {
        return false;
    }
    
    /**
     * Returns this model's set of locations.
     * 
     * @return  This model's set of locations.
     */
    @Override
    public Set<Location> vertexSet() {
        return this.locations;
    }
    
    /**
     * Returns the source of the specified road.
     * 
     * @return  The source of the specified road.
     * @see Road#getSource()
     */
    @Override
    public Location getEdgeSource(Road e) {
        return e.getSource();
    }
    
    /**
     * Returns the destination of the specified road.
     * 
     * @return  The destination of the specified road.
     * @see Road#getDestination()
     */
    @Override
    public Location getEdgeTarget(Road e) {
        return e.getDestination();
    }
    
    /**
     * Returns an estimate of the mean time in seconds for the next car to
     * travel along the specified road.
     * 
     * @param e A road.
     * @return  An estimate of the mean time for the next car to travel along
     *          the specified road.
     * @see Road#getEstimatedMean();
     */
    @Override
    public double getEdgeWeight(Road e) {
        return e.getEstimatedMean();
    }
    
    /**
     * Returns the number of entering roads at the specified location.
     * 
     * @return  The number of entering roads at the specified location.
     * @see Location#getInDegree()
     */
    @Override
    public int inDegreeOf(Location vertex) {
        return vertex.getInDegree();
    }
    
    /**
     * Returns the set of entering roads at the specified location.
     * 
     * @return  The set of entering roads at the specified location.
     * @see Location#getEnteringRoads()
     */
    @Override
    public Set<Road> incomingEdgesOf(Location vertex) {
        return vertex.getEnteringRoads();
    }
    
    /**
     * Returns the number of exiting roads at the specified location.
     * 
     * @return  The number of exiting roads at the specified location.
     * @see Location#getOutDegree()
     */
    @Override
    public int outDegreeOf(Location vertex) {
        return vertex.getOutDegree();
    }
    
    /**
     * Returns a set of exiting roads at the specified location.
     * 
     * @return  A set of exiting roads at the specified location.
     * @see Location#getExitingRoads()
     */
    @Override
    public Set<Road> outgoingEdgesOf(Location vertex) {
        return vertex.getExitingRoads();
    }
    
    /**
     * Does nothing because estimated road travel times should be fixed.
     * 
     * @param e         A road.
     * @param weight    The estimated time to travel the length of the
     *                  specified road.
     */
    @Override
    public void setEdgeWeight(Road e, double weight) {}
    
    /**
     * Sets the A* search heuristic that governs how cars choose their
     * directions of travel.
     * 
     * This should be set after connecting an experiment to this model.
     * 
     * @param heuristic An A* search heuristic.
     */
    public void setHeuristic(AStarSearchHeuristic heuristic) {
        this.heuristic = heuristic;
    }
    
    /**
     * Sets the specified parking lot's exit time distribution to the
     * specified distribution.
     * 
     * @param lot   A parking lot.
     * @param dist  A numerical distribution producing double values.
     * @see ParkingLot#setExitTimeDistribution(NumericalDist)
     */
    public void setExitTimeDistribution(ParkingLot lot,
            NumericalDist<Double> dist) {
        if (null == dist) return;
        dist.changeRandomGenerator(this.getRNG());
        if (this.lots.contains(lot)) {
            lot.setExitTimeDistribution(dist);
        }
    }
    
    /**
     * Sets the specified parking lots' exit time distributions to the
     * specified distribution.
     * 
     * @param lots  A collection of parking lots.
     * @param dist  A numerical distribution producing double values.
     * @see ParkingLot#setExitTimeDistribution(NumericalDist)
     */
    public void setExitTimeDistributions(Collection<ParkingLot> lots,
            NumericalDist<Double> dist) {
        if (null == dist) return;
        dist.changeRandomGenerator(this.getRNG());
        for (ParkingLot lot : lots) {
            if (this.lots.contains(lot)) {
                lot.setExitTimeDistribution(dist);
            }
        }
    }
    
    /**
     * Sets the specified intersection's crossing time distribution to the
     * specified distribution.
     * 
     * @param intersection  An intersection.
     * @param dist          A numerical distribution producing double values.
     * @see Intersection#setCrossingTimeDistribution(NumericalDist)
     */
    public void setCrossingTimeDistribution(Intersection intersection,
            NumericalDist<Double> dist) {
        if (null == dist) return;
        dist.changeRandomGenerator(this.getRNG());
        if (this.intersections.contains(intersection)) {
            intersection.setCrossingTimeDistribution(dist);
        }
    }
    
    /**
     * Sets the specified intersections' crossing time distributions to the
     * specified distribution.
     * 
     * @param intersections A set of intersections.
     * @param dist          A numerical distribution producing double values.
     * @see Intersection#setCrossingTimeDistribution(NumericalDist)
     */
    public void setCrossingTimeDistributions(
            Collection<Intersection> intersections,
            NumericalDist<Double> dist) {
        if (null == dist) return;
        dist.changeRandomGenerator(this.getRNG());
        for (Intersection intersection : intersections) {
            if (this.intersections.contains(intersection)) {
                intersection.setCrossingTimeDistribution(dist);
            }
        }
    }
    
    /**
     * Sets the specified road's speed distribution to the
     * specified distribution.
     * 
     * @param road  A road.
     * @param dist  A numerical distribution producing double values.
     * @see Road#setSpeedDistribution(NumericalDist)
     */
    public void setSpeedDistribution(Road road,
            NumericalDist<Double> dist) {
        if (null == dist) return;
        dist.changeRandomGenerator(this.getRNG());
        if (this.roads.contains(road)) {
            road.setSpeedDistribution(dist);
        }
    }
    
    /**
     * Sets the specified roads' speed distributions to the
     * specified distribution.
     * 
     * @param roads A collection of roads.
     * @param dist  A numerical distribution producing double values.
     * @see Road#setSpeedDistribution(NumericalDist)
     */
    public void setSpeedDistributions(Collection<Road> roads,
            NumericalDist<Double> dist) {
        if (null == dist) return;
        dist.changeRandomGenerator(this.getRNG());
        for (Road road : roads) {
            if (this.roads.contains(road)) {
                road.setSpeedDistribution(dist);
            }
        }
    }
    
    /**
     * Sets the specified parking lot's initial count to the
     * specified count after validation and returns the approximate
     * percentage of initial capacity set.
     * 
     * @param lot   A parking lot.
     * @param count A proposed initial count.
     * @return  The approximate percentage of initial capacity set.
     * @see ParkingLot#setInitialCount(int)
     */
    public double setInitialCount(ParkingLot lot, int count) {
        double val = 0;
        if (this.lots.contains(lot)) {
            val = lot.setInitialCount(count);
            this.results.setInitialLotCounts(lot.getName(), lot.getCapacity(),
                    val);
        }
        return val;
    }
    
    /**
     * Sets the specified parking lots' initial counts to the
     * specified count after validation and returns a list of the approximate
     * percentages of initial capacities set.
     * 
     * 
     * @param lots  A collection of parking lots.
     * @param count A proposed initial count.
     * @return  A list of the approximate percentages of initial capacities
     *          set.
     * @see ParkingLot#setInitialCount(int)
     */
    public List<Double> setInitialCounts(Collection<ParkingLot> lots,
            int count) {
        List<Double> list = new LinkedList<Double>();
        for (ParkingLot lot : lots) {
            if (this.lots.contains(lot)) {
                double val = lot.setInitialCount(count);
                list.add(val);
                this.results.setInitialLotCounts(lot.getName(),
                        lot.getCapacity(), val);
            }
        }
        return list;
    }
    
    /**
     * Sets the specified parking lots' initial counts to a random percentage
     * between the specified bounds and then returns a list of the 
     * percentages set. The random percentages follow a continuous uniform
     * distribution and are values between 0 and 1.
     * 
     * @param lots  A collection of parking lots.
     * @param min   A lower bound on the random percentage.
     * @param max   An upper bound on the random percentage.
     * @return  A list of the approximate percentages of initial capacities
     *          set.
     * @see ParkingLot#setRandomUniformInitialPercent(double, double)
     */
    public List<Double> setRandomUniformInitialPercents(
            Collection<ParkingLot> lots, double min, double max) {
        List<Double> list = new LinkedList<Double>();
        for (ParkingLot lot : lots) {
            if (this.lots.contains(lot)) {
                double val = lot.setRandomUniformInitialPercent(min, max);
                list.add(val);
                this.results.setInitialLotCounts(lot.getName(),
                        lot.getCapacity(), val);
            }
        }
        return list;
    }
    
    /**
     * Sets the specified parking lots' initial counts to a random percentage
     * between 0 and 1 and then returns a list of the percentages set. The
     * initial percentages follow a normal distribution with the specified mean
     * and standard deviation.
     * 
     * @param lots  A collection of parking lots.
     * @param mean  The mean initial percentage.
     * @param stdev The standard deviation of initial percentages.
     * @return  A list of the approximate percentages of initial capacities
     *          set.
     * @see ParkingLot#setRandomNormalInitialPercent(double, double)
     */
    public List<Double> setRandomNormalInitialPercents(
            Collection<ParkingLot> lots, double mean, double stdev) {
        List<Double> list = new LinkedList<Double>();
        for (ParkingLot lot : lots) {
            if (this.lots.contains(lot)) {
                double val = lot.setRandomNormalInitialPercent(mean, stdev);
                list.add(val);
                this.results.setInitialLotCounts(lot.getName(),
                        lot.getCapacity(), val);
            }
        }
        return list;
    }
    
    /**
     * Sets the specified intersection's strategy to a copy of the specified
     * strategy.
     * 
     * @param intersection  An intersection.
     * @param strategy      An intersection strategy.
     * @see Intersection#setStrategy(Strategy)
     */
    public void setStrategy(Intersection intersection, Strategy strategy) {
        if (null == strategy) return;
        if (this.intersections.contains(intersection)) {
            intersection.setStrategy(strategy);
            this.results.setStrategy(intersection.getName(),
                    strategy.getClass().getName());
        }
    }
    
    /**
     * Sets the specified intersections' strategies to a copy of the specified
     * strategy.
     * 
     * @param intersections A collection of intersections.
     * @param strategy      An intersection strategy.
     * @see Intersection#setStrategy(Strategy)
     */
    public void setStrategies(Collection<Intersection> intersections,
            Strategy strategy) {
        if (null == strategy) return;
        for (Intersection intersection : intersections) {
            if (this.intersections.contains(intersection)) {
                intersection.setStrategy(strategy);
                this.results.setStrategy(intersection.getName(),
                        strategy.getClass().getName());
            }
        }
    }
    
    /**
     * Returns this model's set of parking lots.
     * 
     * @return  This model's set of parking lots.
     */
    public Set<ParkingLot> getParkingLots() {
        return this.lots;
    }
    
    /**
     * Returns this model's set of intersections.
     * 
     * @return  This model's set of intersections.
     */
    public Set<Intersection> getIntersections() {
        return this.intersections;
    }
    
    /**
     * Returns this model's set of exits.
     * 
     * @return  This model's set of exits.
     */
    public Set<Exit> getExits() {
        return this.exits;
    }
    
    /**
     * Returns this model's random number generator.
     * 
     * @return  This model's random number generator.
     */
    public UniformRandomGenerator getRNG() {
        return this.rng;
    }
    
    /**
     * Returns the simulation results.
     * 
     * This only works after the simulation has finished.
     * 
     * @return  The simulation results.
     */
    public Results getResults() {
        return results;
    }
    
    /**
     * Returns this model's A* search heuristic.
     * 
     * @return  This model's A* search heuristic.
     */
    public AStarSearchHeuristic<Location, Road> getHeuristic() {
        return this.heuristic;
    }
    
    private void constructWorld() {
        /* NOTE Set<Location> seems unnecessary
         * *each coordinate should correspond to a unique location
         * *each location should correspond to a unique coordinate
         * *each location is a member of the set of locations of the same type
         *      and these sets are all  mutually exclusive
         */
        Map<String, Location> locationref = new HashMap<String, Location>();
        Map<String, Collection<Road>> roadref =
                new HashMap<String, Collection<Road>>();
        int def = -1;
        // Construct locations
        for (String coor : this.info.getAllParkingLotLocations()) {
        	String name = this.info.getParkingLotName(coor);
            int capacity = this.info.getCapacity(name);
            ParkingLot lot = new ParkingLot(this, name, this.traceIsOn(),
                    capacity, def);
            this.lots.add(lot);
            this.locations.add(lot);
            locationref.put(coor, lot);
        }
        for (String name : this.info.getAllIntersectionNames()) {
            Intersection intersection = new Intersection(this, name,
                    this.traceIsOn());
            this.intersections.add(intersection);
            this.locations.add(intersection);
            locationref.put(name, intersection);
        }
        for (String name : this.info.getAllExitNames()) {
            Exit exit = new Exit(this, name, this.traceIsOn());
            this.exits.add(exit);
            this.locations.add(exit);
            locationref.put(name, exit);
        }
        // Construct roads
        for (String name : this.info.getAllRoadNames()) {
            Collection<Road> roads = new HashSet<Road>();
            double length = this.info.getLength(name);
            String[] locations = this.info.getEndsOfRoad(name);
            Location location1 = locationref.get(locations[0]);
            Location location2 = locationref.get(locations[1]);
            String name1 = name + ": from " + location1.getName() + " to "
                    + location2.getName();
            Road road1 = new Road(this, name1, this.traceIsOn(),
                    location1, location2, length);
            String name2 = name + ": from " + location2.getName() + " to "
                    + location1.getName();
            Road road2 = new Road(this, name2, this.traceIsOn(),
                    location2, location1, length);
            roads.add(road1);
            roads.add(road2);
            this.roads.addAll(roads);
            roadref.put(name, roads);
        }
        // Set roads on locations
        for (String name : locationref.keySet()) {
            Location location = locationref.get(name);
            String[] roadNames = this.info.getRoads(name);
            Road[] in = new Road[4];
            Road[] out = new Road[4];
            for (int i = 0; i < 4; i++) {
                if (roadNames[i] != null) {
                    for (Road road : roadref.get(roadNames[i])) {
                        if (road.getDestination() == location) {
                            in[i] = road;
                        } else if (road.getSource() == location) {
                            out[i] = road;
                        }
                    }
                }
            }
            location.setEnteringRoads(in);
            location.setExitingRoads(out);
        }
        // Set a uniform heuristic as the default heuristic
        this.heuristic = new UniformHeuristic(this, this.exits);
    }
    
    private void fillLots() {
        this.setRandomNormalInitialPercents(lots, 0.9, 0.05);
    }
    
    /* 
     * Event routines
     */
    
    /**
     * A car arrives at the end of the specified road. May generate a
     * crossing event or exit map event at the road's destination.
     * 
     * @param road  The road to finish traveling on.
     * @see ArrivalEvent
     * @see CrossingEvent
     * @see ExitMapEvent
     */
    public void arrival(Road road) {
        this.results.notifyArrival(road.getName());
        road.arrive();
        Location location = road.getDestination();
        if (location.isAccessible()) {
            if (location instanceof Intersection) {
                Intersection intersection = (Intersection) location;
                if (intersection.isAccessible() && null != intersection.getNext()) {
                    this.generateCrossingEvent(intersection);
                    if (intersection.getName().equals("(10,0)#1")) {
                        Road next = intersection.getNext();
                        if (next.getName().equals("Road to P2 : from P2 #1 to (10,0)#1#1")) {
                            System.out.println(intersection.getDirection(road));
                        }
                    }
                }
            } else if (location instanceof Exit) {
                Exit exit = (Exit) location;
                this.generateExitMapEvent(road, exit);
            }
        }
    }
    
    /**
     * A car crosses the specified intersection. Generates a traveling event
     * at the specified intersection.
     * 
     * @param intersection  The intersection to cross.
     * @see CrossingEvent
     * @see TravelingEvent
     */
    public void cross(Intersection intersection) {
        this.results.notifyCrossing(intersection.getName(),
                intersection.getCrossString());
        Road source = intersection.getNext();
        Road destination = intersection.getDirection(source);
        this.generateTravelingEvent(source, intersection, destination);
    }
    
    /**
     * A car finishes crossing the specified intersection and begins traveling
     * on the specified destination road. May generate a crossing event at
     * the intersection, and may also generate a crossing event or exit lot
     * event at the source road's origin.
     * 
     * @param source        The entering road exited from.
     * @param intersection  The intersection being crossed.
     * @param destination   The exiting road to enter.
     * @see TravelingEvent
     * @see CrossingEvent
     * @see CarTrafficEvacuationModel#notifyOrigin(Road)
     */
    public void travel(Road source, Intersection intersection,
            Road destination) {
        this.results.notifyTravel(destination.getName());
        boolean full = source.isFull();
        source.decrement();
        destination.increment();
        intersection.cross();
        if (intersection.isAccessible() && null != intersection.getNext()) {
            this.generateCrossingEvent(intersection);
        }
        if (full) {
            this.notifyOrigin(source);
        }
        this.generateArrivalEvent(destination);
    }
    
    /**
     * A car exits the specified parking lot and enters the specified road.
     * Generates an arrival event on the specified road. May also generate a
     * new exit lot event at the specified parking lot.
     * 
     * @param source        The parking lot to exit from.
     * @param destination   The road to enter.
     * @see ExitLotEvent
     * @see ArrivalEvent
     */
    public void exitLot(ParkingLot source, Road destination) {
        this.results.notifyExitLot(source.getName());
        if (source.decrement()) {
            destination.increment();
            this.generateArrivalEvent(destination);
            if (destination.isFull()) {
                source.setInactive(destination);
            } else if (source.getCount() > 0) {
                this.generateExitLotEvent(source, destination);
            }
        }
    }
    
    /**
     * A car exits the simulation map from the specified road into the
     * specified exit. Records the relevant exit information. If the specified
     * road was full prior to the exit, then may generate a crossing event or
     * an exit lot event at the specified road's origin.
     * 
     * @param road  The entering road to exit from.
     * @param exit  The exit to enter.
     * @see ExitMapEvent
     * @see CarTrafficEvacuationModel#notifyOrigin(Road)
     */
    public void exitMap(Road road, Exit exit) {
        this.results.notifyExitMap(exit.getName(), road.getName());
        boolean full = road.isFull();
        road.decrement();
        if (full) {
            this.notifyOrigin(road);
        }
    }
    
    /**
     * Notifies the source of a road of potential in flow. May generate a
     * crossing event or exit lot event at the specified road's origin.
     * 
     * @param road  A road which is no longer full.
     * @see CrossingEvent
     * @see ExitLotEvent
     */
    public void notifyOrigin(Road road) {
        Location origin = road.getSource();
        if (origin instanceof Intersection) {
            Intersection intersection = (Intersection) origin;
            if (intersection.isAccessible() && null != intersection.getNext()) {
                this.generateCrossingEvent(intersection);
            }
        } else if (origin instanceof ParkingLot) {
            ParkingLot lot = (ParkingLot) origin;
            if (!lot.isActive(road)) {
                this.generateExitLotEvent(lot, road);
            }
        }
    }
    
    /*
     * Private event generators.
     */
    
    /**
     * Generate a new arrival event at the specified road.
     * 
     * @param road  The road to finish traveling on.
     * @see ArrivalEvent
     */
    private void generateArrivalEvent(Road road) {
        String str = "arrival at " + road.getDestination() + " along " + road;
        ArrivalEvent event = new ArrivalEvent(this, str, this.traceIsOn());
        TimeSpan time = new TimeSpan(road.sample(), TimeUnit.SECONDS);
        event.schedule(road, time);
    }
    
    /**
     * Generate a new crossing event at the specified intersection.
     * 
     * @param intersection  The intersection to begin crossing at.
     * @see CrossingEvent
     */
    private void generateCrossingEvent(Intersection intersection) {
        String str = "crossing " + intersection + " from "
                + intersection.getNext();
        CrossingEvent event = new CrossingEvent(this, str, this.traceIsOn());
        TimeSpan time = new TimeSpan(0, TimeUnit.SECONDS);
        event.schedule(intersection, time);
        intersection.setActive();
    }
    
    /**
     * Generate a new exit lot event from the specified lot onto the specified
     * road.
     * 
     * @param lot   The parking lot to exit from.
     * @param road  The exiting road to enter.
     * @see ExitLotEvent
     */
    private void generateExitLotEvent(ParkingLot lot, Road road) {
        String str = "exit " + lot + " onto " + road;
        ExitLotEvent event = new ExitLotEvent(this, str, this.traceIsOn());
        TimeSpan time = new TimeSpan(lot.sample(), TimeUnit.SECONDS);
        event.schedule(lot, road, time);
    }
    
    /**
     * Generate a new exit map event from the specified road into the specified
     * exit.
     * 
     * @param road  The entering road to exit from.
     * @param exit  The exit to enter.
     * @see ExitMapEvent
     */
    private void generateExitMapEvent(Road road, Exit exit) {
        String str = "exiting from " + exit + " via " + road;
        ExitMapEvent event = new ExitMapEvent(this, str, this.traceIsOn());
        TimeSpan time = new TimeSpan(0, TimeUnit.SECONDS);
        event.schedule(road, exit, time);
    }
    
    /**
     * Generate a new traveling event from the specified source road, through
     * the specified intersection, and onto the specified destination road.
     * 
     * @param source        The entering road exited from.
     * @param intersection  The intersection being crossed.
     * @param destination   The exiting road to begin traveling on.
     * @see TravelingEvent
     */
    private void generateTravelingEvent(Road source, Intersection intersection,
            Road destination) {
        String str = "traveling on " + destination;
        // DEBUG
        /*
        if (destination == null) {
            System.out.println(intersection.getName());
            System.out.println(source.getName());
        }
        */
        // DEBUG
        //System.out.println(this.presentTime() + ", " + str);
        TravelingEvent event = new TravelingEvent(this, str, this.traceIsOn());
        TimeSpan time = new TimeSpan(intersection.sample(), TimeUnit.SECONDS);
        event.schedule(source, intersection, destination, time);
    }
}
