package model;

import model.util.Counter;
import model.util.Sampleable;
import desmoj.core.dist.Distribution;
import desmoj.core.dist.NumericalDist;
import desmoj.core.dist.UniformRandomGenerator;
import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;

// TODO javadoc class description
/**
 * 
 * Times are in seconds, lengths are in miles, and speeds are in miles per
 * hour.
 * 
 * @author Brian Dong
 *
 */
public class Road extends Entity implements Counter, Sampleable<Double> {
    
    /** The length of this road. */
    private double length;
    /** The capacity of this road. */
    private int capacity;
    /** The number of traveling cars. */
    private int traveling;
    /** The number of waiting cars. */
    private int waiting;
    /** The current estimated mean. */
    private double estimate;
    /** The number of samples generated. */
    private int numSamples;
    /** The source of this road. */
    private Location source;
    /** The destination of this road. */
    private Location destination;
    /** The speed limit of this road. */
    private double speedLimit;
    /** Whether the speeds are fixed. */
    private boolean fixedSpeed;
    /** If the speeds are fixed, then the fixed speed. */
    private double speed;
    /** If the speeds are not fixed, then a distribution of speeds. */
    private NumericalDist<Double> speedDistribution;
    
    // TODO javadoc constructors
    public Road(Model owner, String name, boolean showInTrace, Location source,
            Location destination, double length, double speedLimit,
            int waiting, double speed, NumericalDist<Double> dist) {
        super(owner, name, showInTrace);
        this.source = source;
        this.destination = destination;
        CarTrafficEvacuationModel model = (CarTrafficEvacuationModel) owner;
        this.length = length <= 0 ?
                CarTrafficEvacuationModel.DEFAULT_ROAD_LENGTH : length;
        this.capacity = (int) Math.floor(
                length/CarTrafficEvacuationModel.DEFAULT_CAR_LENGTH);
        this.traveling = 0;
        this.waiting = Math.max(0, Math.min(waiting, this.capacity));
        // TODO estimate mean
        this.estimate = 0;
        this.numSamples = 0;
        this.speedLimit = speedLimit <= 0 ?
                CarTrafficEvacuationModel.DEFAULT_SPEED_LIMIT : speedLimit;
        this.speed = Math.max(0, Math.min(speed, this.speedLimit));
        this.setSpeedDistribution(dist);
    }
    
    public Road(Model owner, String name, boolean showInTrace, Location source,
            Location destination, double length, double speedLimit,
            int waiting, double speed) {
        this(owner, name, showInTrace, source, destination,
                length, speedLimit, waiting, speed, null);
    }
    
    public Road(Model owner, String name, boolean showInTrace, Location source,
            Location destination, double length, double speedLimit,
            int waiting, NumericalDist<Double> dist) {
        this(owner, name, showInTrace, source, destination,
                length, speedLimit, waiting, -1, dist);
    }
    
    public Road(Model owner, String name, boolean showInTrace, Location source,
           Location destination, double length, double speedLimit,
           double speed) {
        this(owner, name, showInTrace, source, destination,
                length, speedLimit, -1, speed, null);
    }
    
    public Road(Model owner, String name, boolean showInTrace, Location source,
            Location destination, double length, double speedLimit,
            NumericalDist<Double> dist) {
        this(owner, name, showInTrace, source, destination,
                length, speedLimit, -1, -1, dist);
    }
    
    public Road(Model owner, String name, boolean showInTrace, Location source,
            Location destination, double length, double speedLimit) {
        this(owner, name, showInTrace, source, destination,
                length, speedLimit, -1, -1, null);
    }
    
    public Road(Model owner, String name, boolean showInTrace, Location source,
            Location destination, double length) {
        this(owner, name, showInTrace, source, destination,
                length, -1, -1, -1, null);
    }
    
    public Road(Model owner, String name, boolean showInTrace, Location source,
            Location destination) {
        this(owner, name, showInTrace, source, destination,
                -1, -1, -1, -1, null);
    }
    
    /**
     * Returns the current number of cars occupying this road.
     * 
     * @return  The current number of cars occupying this road.
     */
    @Override
    public int getCount() {
        return this.traveling + this.waiting;
    }
    
    /**
     * Returns whether this road has reached maximum capacity.
     * 
     * @return  True if this road has reached maximum capacity, false
     *          otherwise.
     */
    @Override
    public boolean isFull() {
        return this.getCount() >= this.capacity;
    }
    
    /**
     * Increments the number of cars occupying this road and returns whether
     * the action was successful.
     * 
     * @return  True if successful, false otherwise.
     */
    @Override
    public boolean increment() {
        boolean success = false;
        if (!this.isFull()) {
            this.traveling++;
            success = true;
        }
        return success;
    }
    
    /**
     * Decrements the number of cars occupying this road and returns whether
     * the action was successful.
     * 
     * @return  True if successful, false otherwise.
     */
    @Override
    public boolean decrement() {
        boolean success = false;
        if (this.waiting > 0) {
            this.waiting--;
            success = true;
            if (this.getCount() == this.capacity - 1) {
                this.source.notifyOutFlow(this);
            }
        }
        return success;
    }
    
    /**
     * Returns an estimate of the mean time in seconds for the next car to
     * travel along this road.
     * 
     * @return  An estimate of the mean time for the next car to travel along
     *          this road.
     */
    @Override
    public Double getEstimatedMean() {
        // TODO update so that its a rolling estimated that is updated
        //      whenever sample() is called
        return this.length/this.convertSpeed(this.speedLimit);
    }
    
    /**
     * Returns the time in seconds for the next car to travel along this road.
     * 
     * @return  The time for the next car to travel along this road.
     */
    @Override
    public Double sample() {
        double speed = this.fixedSpeed ?
                this.speed : Math.min(
                        this.speedDistribution.sample(), this.speedLimit);
        Double sample = this.length/this.convertSpeed(speed);
        return sample;
    }
    
    /**
     * Sets the specified distribution as this road's new speed distribution
     * if possible; otherwise sets the speed distribution to a fixed speed.
     * 
     * Should only be used when initializing the model.
     * Recommended to make the distribution's random number generator the same
     * as the model's random number generator beforehand.
     * 
     * @param dist  The distribution to set as this road's new speed
     *              distribution.
     */
    public void setSpeedDistribution(NumericalDist<Double> dist) {
        if (null != dist) {
            this.fixedSpeed = false;
            this.speedDistribution = dist;
        } else {
            this.fixedSpeed = true;
        }
    }
    
    /**
     * Returns the maximum number of cars that can simultaneously occupy
     * this road.
     * 
     * @return  The capacity of this road.
     */
    public int getCapacity() {
        return this.capacity;
    }
    
    /**
     * Returns the number of cars waiting at the end of this road.
     * 
     * @return  The number of cars waiting at the end of this road.
     */
    public int getWaitingCount() {
        return this.waiting;
    }
    
    /**
     * Returns the source of this road.
     * 
     * @return  The source of this road.
     */
    public Location getSource() {
        return this.source;
    }
    
    /**
     * Returns the destination of this road.
     * 
     * @return  The destination of this road.
     */
    public Location getDestination() {
        return this.destination;
    }
    
    /**
     * Returns the speed limit of this road in miles per hour.
     * 
     * @return  The speed limit of this road.
     */
    public double getSpeedLimit() {
        return this.speedLimit;
    }
    
    /**
     * Returns whether this road has a car waiting to enter the destination.
     * 
     * @return  True if a car is waiting to enter the destination, false
     *          otherwise.
     */
    public boolean hasWaiting() {
        return this.waiting > 0;
    }
    
    /**
     * Allows the next car to finish traveling and arrive at the end of the
     * road. Returns true if this action is successful, otherwise returns
     * false.
     * 
     * @return  True if successful, false otherwise.
     */
    public boolean arrive() {
        boolean success = false;
        if (this.traveling > 0) {
            this.traveling--;
            this.waiting++;
            success = true;
            if (this.waiting == 1) {
                this.destination.notifyInFlow(this);
            }
        }
        return success;
    }
    
    /**
     * Returns the given miles per hour speed as a miles per second speed.
     * 
     * @param speed A speed in miles per hour.
     * @return  The specified speed in miles per second.
     */
    protected double convertSpeed(double speed) {
        return speed/3600;
    }

}