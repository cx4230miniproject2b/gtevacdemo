package model;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import org.jgrapht.Graph;

import desmoj.core.dist.ContDistExponential;
import desmoj.core.dist.ContDistNormal;
import desmoj.core.dist.MersenneTwisterRandomGenerator;
import desmoj.core.dist.NumericalDist;
import desmoj.core.dist.UniformRandomGenerator;
import desmoj.core.simulator.Experiment;
import desmoj.core.simulator.Model;
import model.io.BasicCSVReport;
import model.io.CSVReader;
import model.io.InputReader;
import model.io.ReportWriter;
import model.io.ResultsReport;
import model.io.WorldReader;
import model.strategy.IntersectionStrategy;
import model.strategy.NumCarsOfficerStrategy;
import model.strategy.Strategy;
import model.util.AStarSearchHeuristic;
import model.util.ShortestGoalDistanceHeuristic;

// TODO javadoc for this driver class
public class CarTrafficEvacuationSimulator {
    
    // PARAMETERS TO CHANGE
    
    private static final UniformRandomGenerator rng =
            new MersenneTwisterRandomGenerator(1);
    private static final int seed = 213;
    
    // Random distribution parameters (exponential)
    private static final double exit_time_mean = 10;
    // Random distribution parameters (normal)
    private static final double cross_time_mean = 4;
    private static final double cross_time_stdev = 0.5;
    private static final double speed_mean = 32;
    private static final double speed_stdev = 3;
    
    private static final double lot_mean = 0.9;
    private static final double lot_stdev = 0.05;
    
    // TODO this param not working
    private static final Class<? extends AStarSearchHeuristic>
            heuristic_class = ShortestGoalDistanceHeuristic.class;
    
    // determine which get officer strategies
    private static final double officer_chance = 0.3;
    private static final int officerless_id = Strategy.NO_OFFICER;
    private static final int officer_id = Strategy.NUM_CARS_OFFICER;
    // choose world file
    private static final String fileName = "world";
    
    // choose experiment settings
    private static final TimeUnit display = TimeUnit.MINUTES;
    private static final TimeUnit granularity = TimeUnit.SECONDS;
    private static final boolean officers_on = true;
    
    // ONLY CHANGE ABOVE
    
    public static void main(String[] args) {
        // Load inputs
        InputReader info = new CSVReader(fileName);
        Strategy officerless = null;
        Strategy officer = null;
        final UniformRandomGenerator rng1 = new MersenneTwisterRandomGenerator(seed);
        final UniformRandomGenerator rng2 = new MersenneTwisterRandomGenerator(seed);
        // Run Officerless Simulation
        String name1 = "OfficerlessModel_" + fileName;
        CarTrafficEvacuationModel model1 = new CarTrafficEvacuationModel(
                null, name1, true, true, rng1, info);
        Experiment exp1 = new Experiment(name1, granularity,
                display, null);
        model1.connectToExperiment(exp1);
        setDistributions(model1);
        setInitialCounts(model1);
        setHeuristic(model1);
        setStrategiesRand(model1, 0);
        exp1.start();
        exp1.report();
        exp1.finish();
        ReportWriter writer = new ReportWriter(model1.getResults(),
                name1, ResultsReport.BASIC_CSV); 
        writer.write();
        if (officers_on) {
            // Run Officer Simulation
            String name2 = "OfficerModel_" + fileName;
            CarTrafficEvacuationModel model2 = new CarTrafficEvacuationModel(
                    null, name2, true, true, rng2, info);
            Experiment exp2 = new Experiment(name2, granularity,
                    display, null);
            model2.connectToExperiment(exp2);
            setDistributions(model2);
            setInitialCounts(model2);
            setHeuristic(model2);
            setStrategiesRand(model2, officer_chance);
            exp2.start();
            exp2.report();
            exp2.finish();
            writer = new ReportWriter(model2.getResults(), name2,
                    ResultsReport.BASIC_CSV);
        }
    }
    
    private static void setDistributions(CarTrafficEvacuationModel model) {
        NumericalDist<Double> dist = new ContDistExponential(model,
                "lot exit times", exit_time_mean, model.reportIsOn(),
                model.traceIsOn());
        model.setExitTimeDistributions(model.getParkingLots(), dist);
        dist = new ContDistNormal(model, "crossing times", cross_time_mean,
                cross_time_stdev, model.reportIsOn(), model.traceIsOn());
        model.setCrossingTimeDistributions(model.getIntersections(), dist);
        dist = new ContDistNormal(model, "speeds", speed_mean, speed_stdev,
                model.reportIsOn(), model.traceIsOn());
        model.setSpeedDistributions(model.edgeSet(), dist);
    }
    
    private static void setInitialCounts(CarTrafficEvacuationModel model) {
        Collection<ParkingLot> lots = model.getParkingLots();
        model.setRandomNormalInitialPercents(lots, lot_mean, lot_stdev);
    }
    
    private static void setHeuristic(CarTrafficEvacuationModel model) {
        // TODO get this to work with reflection/generic heuristic input?
        AStarSearchHeuristic heuristic = new ShortestGoalDistanceHeuristic(
                model, model.getExits());
        model.setHeuristic(heuristic);
    }
    
    private static void setStrategiesRand(CarTrafficEvacuationModel model,
            double chance) {
        Collection<Intersection> intersections = new HashSet<Intersection>();
        if (chance > 0) {
            rng.setSeed(seed);
            for (Intersection intersection : model.getIntersections()) {
                if (rng.nextDouble() < chance) {
                    intersections.add(intersection);
                }
            }
        }
        setStrategies(model, intersections);
    }
    
    private static void setStrategiesFixed(CarTrafficEvacuationModel model,
            InputReader info) {
        Collection<Intersection> intersections =
                new LinkedList<Intersection>();
        for (Intersection intersection : model.getIntersections()) {
            if (info.hasOfficer(intersection.getName())) {
                intersections.add(intersection);
            }
        }
        setStrategies(model, intersections);
    }
    
    private static void setStrategies(CarTrafficEvacuationModel model,
            Collection<Intersection> intersections) {
        Collection<Intersection> not = model.getIntersections();
        not.removeAll(intersections);
        Strategy officerless = Strategy.NewStrategy(model, null,
                model.traceIsOn(), officerless_id);
        Strategy officer = Strategy.NewStrategy(model, null,
                model.traceIsOn(), officer_id);
        model.setStrategies(intersections, officer);
        model.setStrategies(not, officerless);
    }
}