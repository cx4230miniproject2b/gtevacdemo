package model;

import java.util.HashSet;
import java.util.Set;

import desmoj.core.simulator.Entity;
import desmoj.core.simulator.Model;

/**
 * An abstract location which fires events upon a car's arrival.
 * 
 * Locations should be initialized before roads. After roads have been
 * initialized, then they can be set at locations.
 * 
 * @author Brian Dong
 *
 */
public abstract class Location extends Entity {
    
    public static final int NORTH = 0;
    public static final int EAST = 1;
    public static final int SOUTH = 2;
    public static final int WEST = 3;
    
    /** The roads entering this location as [N, E, S, W]. */
    protected Road[] in;
    /** The roads exiting this location as [N, E, S, W]. */
    protected Road[] out;
    /** The number of roads entering this location. */
    private int indegree;
    /** The number of roads exiting this location. */
    private int outdegree;

    /**
     * Constructs a new location. 
     * 
     * Locations should be initialized before roads. After roads have been
     * initialized, then they can be set at locations.
     * 
     * @param owner         The model this location belongs to.
     * @param name          The name of this location.
     * @param showInTrace   Whether this location is traceable.
     */
    public Location(Model owner, String name, boolean showInTrace) {
        super(owner, name, showInTrace);
        this.in = null;
        this.out = null;
        this.indegree = 0;
        this.outdegree = 0;
    }
    
    /**
     * Returns a set of roads entering this location.
     * 
     * @return  A set of roads entering this location.
     */
    public Set<Road> getEnteringRoads() {
        return this.arrayToSet(this.in);
    }
    
    /**
     * Returns a set of roads exiting this location.
     * 
     * @return  A set of roads exiting this location.
     */
    public Set<Road> getExitingRoads() {
        return this.arrayToSet(this.out); 
    }
    
    /**
     * Returns the number of roads entering this location.
     * 
     * @return  The number of roads entering this location.
     */
    public int getInDegree() {
        return this.indegree;
    }
    
    /**
     * Returns the number of roads exiting this location.
     * 
     * @return  The number of roads exiting this location.
     */
    public int getOutDegree() {
        return this.outdegree;
    }
    
    /**
     * Sets the entering roads for this location. The roads should be an array
     * [N, E, S, W], with null if a road does not exist.
     * 
     * This should only be used once right after initialization.
     * 
     * @param roads An array of roads.
     */
    public void setEnteringRoads(Road[] roads) {
        this.in = roads;
        for (Road road : roads) {
            if (null != road) this.indegree++;
        }
    }
    
    /**
     * Sets the exiting roads for this location. The roads should be an array
     * [N, E, S, W], with null if a road does not exist.
     * 
     * This should only be used once right after initialization.
     * 
     * @param roads An array of roads.
     */
    public void setExitingRoads(Road[] roads) {
        this.out = roads;
        for (Road road : roads) {
            if (null != road) this.outdegree++;
        }
    }
    
    /**
     * Returns whether this location can be accessed by a car at this time.
     * 
     * @return  True if this location can be accessed at this time,  false
     *          otherwise.
     */
    public abstract boolean isAccessible();
    
    /**
     * Notifies this location of an arrival from the specified road.
     * 
     * @param road  The road a car is arriving from.
     */
    public abstract void notifyInFlow(Road road);
    
    /**
     * Notifies this location of potential traffic flow out into the
     * specified road.
     * 
     * @param road  A road that is no longer full.
     */
    public abstract void notifyOutFlow(Road road);
    
    /**
     * Converts the given array of roads into a set of roads by removing null
     * entries in the array. Returns the resulting set.
     * 
     * @param roads An array of roads.
     * @return  A set of roads.
     */
    private Set<Road> arrayToSet(Road[] roads) {
        Set<Road> set = new HashSet<Road>();
        for (Road r : roads) {
            if (null != r) {
                set.add(r);
            }
        }
        return set;
    }
    
}