package model;

import model.util.Counter;
import model.util.Sampleable;
import desmoj.core.dist.ContDistNormal;
import desmoj.core.dist.Distribution;
import desmoj.core.dist.NumericalDist;
import desmoj.core.dist.UniformRandomGenerator;
import desmoj.core.simulator.Model;

// TODO class description
public class ParkingLot extends Location implements Counter, Sampleable<Double> {
    
    /** The maximum number of cars that can be contained. */
    private int capacity;
    /** The number of cars in the parking lot. **/
    private int count;
    /** The current estimated mean. */
    private double estimate;
    /** The number of samples generated. */
    private int numSamples;
    /** Whether the exit times are fixed. */
    private boolean fixedTimes;
    /** If the exit times is fixed, then the time it takes to exit. */
    private double exitTime;
    /** If the exit times is not fixed, then a distribution of exit times. */
    private NumericalDist<Double> exitTimeDistribution;
    /** An array of booleans corresponding to [N, E, S, W]
     *  and an entry is true if the corresponding road is not full. */
    private boolean[] active;
    // TODO replace active array with map?, time vs space
    /** The current estimated mean. */

    // TODO javadoc constructors
    /**
     * Constructs a parking lot from the given inputs.
     * 
     * Parking lots should be initialized before roads. After roads have been
     * initialized, then they can be set at parking lots.
     * 
     * @param owner         
     * @param name          
     * @param showInTrace   
     * @param capacity      
     * @param initialCount  
     * @param fixedTimes    
     * @param exitTime      
     * @param dist          
     */
    public ParkingLot(Model owner, String name, boolean showInTrace,
            int capacity, int initialCount,
            double exitTime, NumericalDist<Double> dist) {
        super(owner, name, showInTrace);
        this.capacity = capacity <= 0 ?
                CarTrafficEvacuationModel.DEFAULT_CAPACITY : capacity;
        this.count = this.validateCount(initialCount);
        this.estimate = 0; // TODO estimate mean
        this.numSamples = 0;
        this.exitTime = exitTime < 0 ?
                CarTrafficEvacuationModel.DEFAULT_EXIT_TIME : exitTime;
        this.setExitTimeDistribution(dist);
        this.active = new boolean[]{false, false, false, false};
    }
    
    public ParkingLot(Model owner, String name, boolean showInTrace,
            int capacity, int initialCount, double exitTime) {
        this(owner, name, showInTrace, capacity, initialCount, exitTime, null);
    }
    
    public ParkingLot(Model owner, String name, boolean showInTrace,
            int capacity, int initialCount, NumericalDist<Double> dist) {
        this(owner, name, showInTrace, capacity, initialCount, -1, dist);
    }
    
    public ParkingLot(Model owner, String name, boolean showInTrace,
            int capacity, double exitTime) {
        this(owner, name, showInTrace, capacity, -1, exitTime, null);
    }
    
    public ParkingLot(Model owner, String name, boolean showInTrace,
            int capacity, NumericalDist<Double> dist) {
        this(owner, name, showInTrace, capacity, -1, -1, dist);
    }
    
    public ParkingLot(Model owner, String name, boolean showInTrace,
            double exitTime) {
        this(owner, name, showInTrace, -1, -1, exitTime, null);
    }
    
    public ParkingLot(Model owner, String name, boolean showInTrace,
            NumericalDist<Double> dist) {
        this(owner, name, showInTrace, -1, -1, -1, dist);
    }
    
    public ParkingLot(Model owner, String name, boolean showInTrace) {
        this(owner, name, showInTrace, -1, -1, -1, null);
    }
    
    /**
     * Returns the current number of cars occupying this parking lot.
     * 
     * @return  The current number of cars occupying this parking lot.
     */
    @Override
    public int getCount() {
        return this.count;
    }
    
    /**
     * Returns whether this parking lot has reached maximum capacity.
     * 
     * @return  True if this parking lot has reached maximum capacity,
     *          false otherwise.
     */
    @Override
    public boolean isFull() {
        return this.count >= this.capacity;
    }
    
    /**
     * Increments the number of cars occupying this parking lot and returns
     * whether the action was successful.
     * 
     * @return  True if successful, false otherwise.
     */
    @Override
    public boolean increment() {
        boolean success = false;
        if (!this.isFull()) {
            this.count++;
            success = true;
        }
        return success;
    }
    
    /**
     * Decrements the number of cars occupying this parking lot and returns
     * whether the action was successful.
     * 
     * @return  True if successful, false otherwise.
     */
    @Override
    public boolean decrement() {
        boolean success = false;
        if (this.count > 0) {
            this.count--;
            success = true;
        }
        return success;
    }
    
    /**
     * Returns an estimate of the mean time in seconds until the next car may
     * exit.
     * 
     * @return  An estimate of the mean time until the next car may exit.
     */
    @Override
    public Double getEstimatedMean() {
        // TODO update so that its a rolling estimated that is updated
        //      whenever sample() is called
        return this.exitTime;
    }
    
    /**
     * Returns the time in seconds until the next car may exit.
     * 
     * @return  The time until next car may exit.
     */
    @Override
    public Double sample() {
        Double sample = (Double) (this.fixedTimes ?
                this.exitTime : this.exitTimeDistribution.sample());
        return sample;
    }
    
    @Override
    public void setExitingRoads(Road[] roads) {
        super.setExitingRoads(roads);
        for (int i = 0; i < 4; i++) {
            if (null != roads[i]) {
                this.active[i] = !roads[i].isFull();
            }
        }
    }
    
    @Override
    public boolean isAccessible() {
        return !this.isFull();
    }
    
    /**
     * Does nothing because this operation is not defined.
     * 
     * @param road  An entering road.
     */
    @Override
    public void notifyInFlow(Road road) {}

    @Override
    public void notifyOutFlow(Road road) {
        boolean found = false;
        int i = 0;
        while (i < 4 && !found) {
            found = road == this.out[i];
            i++;
        }
        if (found) this.active[i-1] = true;
    }
    
    /**
     * Sets the specified distribution as this parking lot's new exit time
     * distribution if possible; otherwise sets the exit time distribution to
     * a fixed time.
     * 
     * Should only be used when initializing the model.
     * Recommended to make the distribution's random number generator the same
     * as the model's random number generator beforehand.
     * 
     * @param dist  The distribution to set as this parking lot's new exit
     *              time distribution.
     */
    public void setExitTimeDistribution(NumericalDist<Double> dist) {
        if (null != dist) {
            this.fixedTimes = false;
            this.exitTimeDistribution = dist;
        } else {
            this.fixedTimes = true;
        }
    }
    
    /**
     * Sets the specified road as inactive.
     * 
     * @param road  An exiting road.
     */
    public void setInactive(Road road) {
        boolean found = false;
        int i = 0;
        while (i < 4 && !found) {
            found = road == this.out[i];
            i++;
        }
        if (found) this.active[i-1] = false;
    }
    
    /**
     * Sets this parking lot's initial count to the specified count after
     * rounding it to the nearest valid value.
     * 
     * @param count A proposed initial count.
     * @return  The approximate percentage of initial capacity set.
     */
    public double setInitialCount(int count) {
        this.count = this.validateCount(count);
        double val = ((double) this.count)/this.capacity;
        return val;
    }
    
    /**
     * Sets this parking lot's initial count to a random percentage between
     * the specified bounds and then returns the value. The random percentage
     * follows a continuous uniform distribution and is a value between 0 and
     * 1.
     * 
     * Should only be called when initializing the model.
     * 
     * @param min   A lower bound on the random percentage.
     * @param max   An upper bound on the random percentage.
     * @return  The approximate percentage of initial capacity set.
     */
    public double setRandomUniformInitialPercent(double min, double max) {
        CarTrafficEvacuationModel model =
                (CarTrafficEvacuationModel) this.getModel();
        double val = min + (max - min)*model.getRNG().nextDouble();
        this.count = (int) Math.round(this.capacity*val);
        return this.count/(double) this.capacity;
    }
    
    /**
     * Sets this parking lot's initial count to a random percentage between 0
     * and 1 and returns the value. The initial count follows a normal
     * distribution with the specified mean and standard deviation.
     * 
     * @param mean  The mean initial count.
     * @param stdev The standard deviation of initial counts.
     * @return  The approximate percentage of initial capacity set.
     */
    public double setRandomNormalInitialCount(double mean, double stdev) {
        CarTrafficEvacuationModel model =
                (CarTrafficEvacuationModel) this.getModel();
        NumericalDist<Double> dist = new ContDistNormal(model, null, mean,
                stdev, model.reportIsOn(), model.traceIsOn());
        this.count = this.validateCount(dist.sample());
        double val = ((double) this.count)/this.capacity;
        return val;
    }
    
    /**
     * Sets this parking lot's initial count to a random percentage between 0
     * and 1 and returns the value. The initial percentage follows a normal
     * distribution with the specified mean and standard deviation.
     * 
     * @param mean  The mean initial percentage.
     * @param stdev The standard deviation of initial percentages.
     * @return  The approximate percentage of initial capacity set.
     */
    public double setRandomNormalInitialPercent(double mean, double stdev) {
        CarTrafficEvacuationModel model =
                (CarTrafficEvacuationModel) this.getModel();
        NumericalDist<Double> dist = new ContDistNormal(model, null, mean,
                stdev, model.reportIsOn(), model.traceIsOn());
        double val = dist.sample();
        this.count = this.validateCount(this.capacity*val);
        return this.count/(double) this.capacity;
    }
    
    /**
     * Returns this parking lot's capacity.
     * 
     * @return  This parking lot's capacity.
     */
    public int getCapacity() {
        return this.capacity;
    }
    
    /**
     * Returns whether the specified road contains exiting traffic from this
     * this location.
     * 
     * @param road  An exiting road.
     * @return  True if the road contains exiting traffic from this location,
     *          false otherwise.
     */
    public boolean isActive(Road road) {
        boolean active = false;
        boolean found = false;
        int i = 0;
        while (i < 4 && !found) {
            found = (road == this.out[i]);
            i++;
        }
        if (found) active = this.active[i-1];
        return active;
    }
    
    /**
     * Validates the proposed initial count by rounding it to the nearest
     * integer between 0 and this parking lot's capacity.
     * 
     * @param count A proposed initial count.
     * @return  A validated initial count.
     */
    private int validateCount(double count) {
        return (int) Math.round(Math.max(0, Math.min(count, this.capacity)));
    }
}