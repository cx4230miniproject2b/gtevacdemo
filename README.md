A discrete event simulator for traffic beginning in parking lots and evacuating an area. For more details read the problem statement and plan.

Desmo-J, a discrete-event modeling and simulation framework, is used by this project and its source code is stored in src/desmoj

Statistics packages from Apache Commons is used in this project and its source code is stored in src/org.apache. 

JGraphT is Java graph library used, with source code stored in src/org.jgrapht

The implementation for the traffic evacuation simulation is in src/model
The class CarTrafficEvacuationSimulator is the driver class responsible for instantiating and running the model. The model is connected to an experiment using the Desmo-J library which handles the discrete events generation.  Model and experiment parameters can be modified in the driver, but the actual world model itself needs to be read in from an input .csv file (examples are provided in the root).

The CarTrafficEvaluationModel is responsible for maintaining the different components of the model, such as the roads and intersections. Parameters and initial conditions are used here

Each intersection is assigned a strategy for directing the flow of traffic. Strategies are in src/model/strategies.  Currently only NumCarsOfficerStrategy and IntersectionStrategy work.  The others are untested.

Input is handled through reading a csv file. The csv file that is used to set-up the world is stored as world.csv and the main file responsible for reading it is WorldReader.  Output is saved to a .csv file. The files that handle input and output for the simulation are in src/model/io

Other assisting functionalities are stored in src/model/util. PRNGTest includes the test that was used for the Mersenne Twister pseudo-random number generator. The other files such as AStarSearchHeuristic are responsible for the A* search that is used to search for the cost of a car getting to an exit via different roads.